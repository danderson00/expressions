export interface ExpressionObservable {
  accumulate: (property: string) => Expression
}

type Expression = (ExpressionObservable) => ExpressionObservable

export interface VocabularyMap {
  [key: string]: Expression
}
