const deepEqual = require('./deepEqual')

const utils = module.exports = {
  unwrap: function (value) {
    if(!value) {
      return value
    } else if(value && typeof value.subscribe === 'function' && typeof value.unsubscribe === 'function') {
      return utils.unwrap(value())
    } else if(typeof value === 'object') {
      if(value.constructor === Array) {
        return value.map(utils.unwrap)
      } else {
        return Object.keys(value).reduce(
          (result, name) => ({ ...result, [name]: utils.unwrap(value[name]) }),
          {}
        )
      }
    } else {
      return value
    }
  },

  evaluateExpression: function (expression, inputValue) {
    switch(typeof expression) {
      case 'function':
        return expression(inputValue)
      case 'undefined':
        return inputValue
      default:
        return inputValue[expression]
    }
  },

  isObservable: o => typeof o === 'function' &&
    typeof o.subscribe === 'function' &&
    typeof o.unsubscribe === 'function',

  uid: (current => () => ++current)(0),

  flat: array => (
    (array && array.constructor === Array)
      ? array.reduce(
          (result, element) => [...result, ...utils.flat(element)],
          []
        )
      : array === undefined
        ? []
        : [array]
  ),

  deepEqual
}