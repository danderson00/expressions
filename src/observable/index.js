const source = require('@x/observable/src/source')
const fromEmitter = require('@x/observable/src/fromEmitter')
const merge = require('@x/observable/src/merge')
const swappable = require('@x/observable/src/swappable')
const serializable = require('./serializable')
const errorObservable = require('./errorObservable')
const operatorContainer = require('./operatorContainer')

module.exports = serializable
Object.assign(module.exports, {
  ...source(serializable),
  ...merge(serializable),
  fromEmitter: fromEmitter(serializable),
  swappable: swappable(serializable),
  operatorContainer: operatorContainer(serializable),
  errorObservable
})
