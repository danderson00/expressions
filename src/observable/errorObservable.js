const observable = require('@x/observable/src/observable')

module.exports = (parent, context, options) => observable(
  (publish, o) => {
    const parentSubscription = parent && parent.subscribe(publish)
    let finalContext = context
    let sourceSubscriptions = []

    o.publish = publish
    o.report = error => publish({ frames: [finalContext], error })

    // this is a *non-destructive* operation - it will not overwrite existing values
    o.addContext = additionalContext => finalContext = { ...additionalContext, ...finalContext }

    o.addSource = (source, context) => {
      const subscription = source.errorObservable.subscribe(
        ({ frames, error }) => publish({ frames: [{ ...finalContext, ...context }, ...frames], error })
      )
      sourceSubscriptions = [...sourceSubscriptions, { source, subscription }]
      return source
    }

    o.removeSource = source => {
      const sourceSubscription = sourceSubscriptions.find(x => x.source === source)
      // TODO: resolve potential memory leak
      //       all tests in core pass without the following test, but downstream tests fail (socket/socket)
      //       not likely to be an issue but could cause old refs in groupBy etc to be retained
      if(sourceSubscription) {
        sourceSubscription.subscription.unsubscribe()
        sourceSubscriptions = sourceSubscriptions.filter(x => x !== sourceSubscription)
      }
    }

    o.disconnect = () => {
      parentSubscription && parentSubscription.unsubscribe()
      sourceSubscriptions.forEach(x => x.unsubscribe())
    }
  },
  options
)
