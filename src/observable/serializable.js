const observable = require('@x/observable/src/observable')
const errorObservable = require('./errorObservable')

module.exports = (publisher = () => {}, options = {}) => observable(
  (publish, container, context) => {
    const {
      parent, definition: providedDefinition, getState, getDefinition, executionContext, returns, source
    } = options

    const definition = {
      returns: returns || 'stream',
      ...(executionContext && { executionContext }),
      ...(source && { source }),
      ...providedDefinition
    }

    container.getState = () => getState
      ? getState({ currentValue: container() })
      : { currentValue: container() }

    container.errorObservable = errorObservable(parent && parent.errorObservable)

    container.getSerializationInfo = () => ({
      definition: getDefinition ? getDefinition(definition) : definition,
      state: container.getState(),
      error: container.errorObservable()
    })

    container.parent = parent

    return publisher(publish, container, context)
  },
  options
)
