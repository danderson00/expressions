const typeAnnotation = Symbol()

module.exports = serializable => (parent, publisher, options) => serializable(
  (publish, o, { getSubscriptions }) => {
    o[typeAnnotation] = true

    let disconnected = false
    const registrations = []
    const registerSubscription = subscription => {
      registrations.push(subscription)
      return subscription
    }

    publisher(publish, registerSubscription, o)

    const unsubscribe = () => {
      registrations.forEach(x => x.unsubscribe())
      disconnected = true
    }

    return () => {
      if(!disconnected) {
        if (parent && parent[typeAnnotation]) {
          if (parent.disconnect() === false) {
            unsubscribe()
          }
        } else if (getSubscriptions().length > 1) {
          return false
        } else {
          unsubscribe()
        }
      }
    }
  },
  { parent, ...options }
)