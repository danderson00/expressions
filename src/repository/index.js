const container = require('./container')
const { unpack, shallow } = require('./vocabulary')

module.exports = ({ vocabularyOperator, builtIn }) => {
  const operators = container(builtIn)
  const vocabularyList = []

  return {
    ...operators,
    addVocabulary: vocabularyDefinition => unpack(vocabularyDefinition)
      .forEach(vocabulary => {
        vocabularyList.push(vocabulary)
        operators.add(vocabularyOperator(vocabulary))
      }),
    // TODO: this is a relatively expensive operation - cache either here or in the host
    getVocabularyShallow: () => vocabularyList.map(vocabulary => shallow(vocabulary, operators))
  }
}
