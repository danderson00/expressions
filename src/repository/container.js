const { proxy } = require('../observable')

module.exports = initial => {
  const operators = { ...initial }

  const container = {
    add: (component) => operators[component.type] = [...(operators[component.type] || []), component],
    find: (type, id) => operators[type].find(x => x.identifier === id),
    forType: type => operators[type],
    extendObservable,
    createExpression: (inputObservable, options) => extendObservable(
      proxy(inputObservable, { source: true, ...options })
    )
  }

  /*
  Direct consumers of extendObservable should either provide a disconnect function or be responsible for not consuming
  messages in an error state

  Eventually, there will be a formal `expressionContainer` (not to be confused with
  `operators/core/expressionContainer`) that will serve as the errorObservable "host" like here, but will also serve as
  a more formal serialization container / boundary. The current approach of using `{ source: true }` in an observable
  definition to tell the serializer to stop walking up the observable chain is somewhat flaky and unclear.
  This expression container will keep a reference to each serializable as they are created and can cache the
  result of costly serialization operations to optimise the process.

  extendObservable should also be eventually removed and the creation of new expression containers preferred
  */
  function extendObservable(inputObservable) {
    const returnType = observable => (
      (observable.getSerializationInfo && observable.getSerializationInfo().definition.returns) || 'stream'
    )

    return innerExtendObservable(inputObservable)

    function innerExtendObservable(target, position = 1) {
      operators[returnType(target)].forEach(operator => {
        target[operator.identifier] = (...args) => {
          const definition = operator.component(target, container)
          const instance = definition.create(...args)
          instance.errorObservable.addContext({
            operator: operator.identifier,
            position
          })
          return innerExtendObservable(instance, position + 1)
        }
      })
      return target
    }
  }

  return container
}