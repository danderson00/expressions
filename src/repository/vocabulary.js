const observable = require('../observable')
const { getFunctionParameters } = require('../serialization')
const { unwrap } = require('../utilities')

module.exports = {
  unpack: function (vocabulary) {
    // allow adding single vocabulary definitions
    if(vocabulary.name && vocabulary.expression) {
      return [vocabulary]
    }

    // otherwise treat the argument as a hash of definitions
    // unpack the definitions and flatten them
    return Object.keys(vocabulary).reduce(
      (result, name) => [...result, ...unpackVocabularyHashValue(name, vocabulary[name])],
      []
    )
  },
  // contains enough information for a "shallow" operator to be created in place of the actual vocabulary
  shallow: (vocabulary, operators) => {
    const result = vocabulary.expression(operators.createExpression(observable()))
    const returns = result.getSerializationInfo().definition.returns

    return {
      name: vocabulary.name,
      defaultValue: unwrap(result),
      parameters: getFunctionParameters(vocabulary.expression).slice(1),
      returns
    }
  }
}

function unpackVocabularyHashValue(name, vocabulary) {
  if(vocabulary instanceof Array) {
    return [
      { name, type: 'stream', returns: name, expression: vocabulary[0] },
      ...Object.keys(vocabulary[1]).map(childName => ({
        name: childName, 
        type: name, 
        expression: vocabulary[1][childName]
      }))
    ]
  } else if (vocabulary instanceof Function) {
    return [{ 
      name, 
      expression: vocabulary
    }]
  } else {
    return [vocabulary]
  }
}
