const expressionContainer = require('./core/expressionContainer')
const { extractReturnType, getFunctionParameters } = require('../serialization')

module.exports = vocabulary => ({
  type: vocabulary.type || 'stream',
  identifier: vocabulary.name,
  component: (inputObservable, operators) => {
    // see serialization/functionParameters for discussion on obtaining function parameter names
    const getClosures = values => getFunctionParameters(vocabulary.expression).slice(1).reduce(
      (closures, parameterName, index) => ({ ...closures, [parameterName]: values[index] }),
      {}
    )

    return {
      create: (...args) => expressionContainer.create(operators, inputObservable, vocabulary.expression, args, {
        type: 'stream',
        identifier: vocabulary.name,
        returns: vocabulary.returns || extractReturnType(operators, vocabulary.expression),
        parameters: args,
        executionContext: { closures: getClosures(args) }
      }),
      unpack: info => expressionContainer.unpack(operators, inputObservable, info, vocabulary.expression)
    }
  }
})