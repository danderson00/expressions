const construct = require('./construct')

module.exports = {
  builtIn: {
    stream: [
      require('./stream/accumulate'),
      require('./stream/all'),
      require('./stream/assign'),
      require('./stream/average'),
      require('./stream/compose'),
      require('./stream/count'),
      require('./stream/first'),
      require('./stream/groupBy')(false),
      require('./stream/groupBy')(true),
      require('./stream/map'),
      require('./stream/reduce'),
      require('./stream/select'),
      require('./stream/sum'),
      require('./stream/take'),
      require('./stream/takeLast'),
      require('./stream/tap'),
      require('./stream/topic'),
      require('./stream/where')('where'),
      require('./stream/where')('filter'),
      construct('stream')
    ],
    aggregate: [
      require('./aggregate/asHash'),
      require('./aggregate/average'),
      require('./aggregate/count'),
      require('./aggregate/map'),
      require('./aggregate/mapAll'),
      require('./aggregate/orderBy'),
      require('./aggregate/orderByDescending'),
      require('./aggregate/sum'),
      require('./aggregate/where')('where'),
      require('./aggregate/where')('filter'),
      construct('aggregate')
    ]
  },
  vocabularyOperator: require('./vocabulary')
}