module.exports = type => ({
  type,
  identifier: 'construct',
  component: (inputObservable, operators) => ({
    create: (id, ...args) => {
      const definition = operators.find(type, id)

      if(!definition) {
        throw new Error(`No such operator ${id} for type ${type}`)
      }

      const o = definition.component(inputObservable, operators).create.apply(null, args)
      o.errorObservable.addContext({ operator: id })
      return o
    },
    unpack: () => {
      throw new Error(`construct operators should never be deserialized directly`)
    }
  })
})