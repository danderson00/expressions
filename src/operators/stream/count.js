const { reduce } = require('../../streams').streams
const pure = require('../core/pure')

module.exports = pure(
  'stream',
  'count', 
  'stream',
  reduce, 
  () => [count => count + 1],
  0
)
