const { filter } = require('../../streams').streams
const pure = require('../core/pure')

module.exports = name => pure(
  'stream',
  name,
  'stream',
  filter, 
  expression => [expression]
)
