const { map } = require('../../streams').streams
const pure = require('../core/pure')

module.exports = pure(
  'stream',
  'tap',
  'stream',
  map, 
  sink => [inputValue => {
    sink(inputValue)
    return inputValue
  }]
)
