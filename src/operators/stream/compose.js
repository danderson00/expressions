const { compose } = require('../../streams').streams
const { serialize, deserialize, extractDefinition, frames } = require('../../serialization')

// this could be simplified vastly by using expressionContainer
module.exports = {
  type: 'stream',
  identifier: 'compose',
  component: (inputObservable, operators) => {
    return {
      create: function(expressions, aggregator) {
        let actualExpressions
        let actualAggregator

        actualExpressions = Array.from(arguments).slice(0, arguments.length - 1)
        actualAggregator = arguments[arguments.length - 1]

        const observables = actualExpressions.map(
          expression => expression(
            operators.createExpression(inputObservable)
          )
        )

        return create(actualExpressions, observables, actualAggregator)
      },
      unpack: ({ definition, state }) => {
        return create(
          definition.expressions,
          definition.expressions.map((expression, index) => (
            deserialize(
              operators,
              operators.createExpression(inputObservable),
              {
                frames: frames.zip(
                  extractDefinition(operators, expression),
                  state && state.expressions[index]
                )
              }
            )
          )),
          definition.aggregator,
          state && state.currentValue,
        )
      }
    }

    function create(expressions, observables, aggregator) {
      return compose(
        inputObservable,
        {
          definition: {
            type: 'stream',
            identifier: 'compose',
            returns: 'stream',
            expressions,
            aggregator
          },
          getState: ({ currentValue }) => ({
            expressions: observables.map(o => serialize(o, true).frames),
            currentValue
          })
        },
        observables,
        aggregator
      )
    }
  }
}