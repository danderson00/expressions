const { reduce } = require('../../streams').streams
const pure = require('../core/pure')

module.exports = pure(
  'stream',
  'take',
  'aggregate',
  reduce, 
  quantity => [(currentEntries, nextValue) => [...currentEntries, nextValue].slice(0, quantity || 1)],
  []
)