const { assign } = require('../../streams').streams
const { serialize, deserialize, extractDefinition, frames } = require('../../serialization')

// much of this is copied from compose, potential refactor involving expressionContainer here
module.exports = {
  type: 'stream',
  identifier: 'assign',
  component: (inputObservable, operators) => {
    return {
      create: function(expressions) {
        const observables = Object.keys(expressions).reduce(
          (result, key) => ({
            ...result,
            [key]: expressions[key](
              operators.createExpression(inputObservable)
            )
          }),
          {}
        )

        return create(expressions, observables)
      },
      unpack: ({ definition, state }) => {
        return create(
          definition.keys.reduce(
            (expressions, key, index) => ({
              ...expressions,
              [key]: definition.expressions[index]
            }),
            {}
          ),
          definition.keys.reduce(
            (observables, key, index) => ({
              ...observables,
              [key]: deserialize(
                operators,
                operators.createExpression(inputObservable),
                {
                  frames: frames.zip(
                    extractDefinition(operators, definition.expressions[index]),
                    state && state.expressions[index]
                  )
                },
              ),
            }),
            {}
          )
        )
      }
    }

    function create(expressions, observables) {
      return assign(
        inputObservable,
        {
          definition: {
            type: 'stream',
            identifier: 'assign',
            returns: 'stream',
            keys: Object.keys(expressions),
            expressions: Object.values(expressions)
          },
          getState: ({ currentValue }) => ({
            expressions: Object.values(observables).map(o => serialize(o, true).frames),
            currentValue
          })
        },
        observables
      )
    }
  }
}