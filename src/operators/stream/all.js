const { reduce } = require('../../streams').streams
const pure = require('../core/pure')

module.exports = pure(
  'stream',
  'all', 
  'aggregate',
  reduce, 
  () => [(currentEntries, nextValue) => [nextValue, ...currentEntries]],
  []
)