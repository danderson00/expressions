const { filter } = require('../../streams').streams
const pure = require('../core/pure')

module.exports = pure(
  'stream',
  'topic', 
  'stream',
  filter, 
  (...args) => [x => x && args.includes(x.topic)]
)
