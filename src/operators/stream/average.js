const { reduce } = require('../../streams').streams
const stateful = require('../core/stateful')
const { evaluateExpression } = require('../../utilities')

module.exports = stateful(
  'stream',
  'average', 
  'stream',
  reduce, 
  expression => (state, currentValue, nextValue) => {
    const sum = state.sum + evaluateExpression(expression, nextValue)
    const count = state.count + 1
    return {
      state: { sum, count },
      value: sum / count
    }
  },
  {
    sum: 0,
    count: 0
  }
)
