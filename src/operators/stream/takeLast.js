const { reduce } = require('../../streams').streams
const pure = require('../core/pure')

module.exports = pure(
  'stream',
  'takeLast',
  'aggregate',
  reduce, 
  quantity => [(currentEntries, nextValue) => [nextValue, ...currentEntries].slice(0, quantity || 1)],
  []
)