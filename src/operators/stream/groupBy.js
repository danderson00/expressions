const { groupBy } = require('../../streams').streams
const { subject } = require('../../observable')
const { serialize, deserialize, extractDefinition, getFunctionParameters } = require('../../serialization')
const { serialize: serializeDefinition } = require('../../serialization/definition')

module.exports = hashBy => ({
  type: 'stream',
  identifier: hashBy ? 'hashBy' : 'groupBy',
  component: (inputObservable, operators) => {
    return {
      create,
      unpack: ({ definition, state }) => create(
        definition.expression, 
        definition.groupExpression,
        definition.removeExpression,
        state
      )
    }

    function create(expression, groupExpression, removeExpression, state) {
      const resolvedGroupExpression = resolveGroupExpression(groupExpression)
      const groupExpressionDefinition = groupExpression && extractDefinition(operators, resolvedGroupExpression)
      const groupExpressionParameters = groupExpression && getFunctionParameters(resolvedGroupExpression)
      const extendedGroupExpression = groupExpression && ((groupObservable, key) => (
        resolvedGroupExpression(operators.extendObservable(groupObservable), key)
      ))

      const extendedRemoveExpression = removeExpression && (
        groupObservable => removeExpression(operators.extendObservable(groupObservable))
      )

      const unpackedGroups = state && unpackGroups(state.currentValue)

      const result = groupBy(
        inputObservable,
        {
          initialValue: unpackedGroups,
          hashBy,
          definition: {
            type: 'stream',
            identifier: 'groupBy',
            returns: hashBy ? 'stream' : 'aggregate',
            expression,
            groupExpression,
            removeExpression
          },
          getState: state => ({
            currentValue: state ? packGroups(state.currentValue) : []
          })
        },
        expression,
        extendedGroupExpression,
        extendedRemoveExpression
      )

      unpackedGroups && unpackedGroups.forEach(({ key, observable, removeObservable }) => {
        result.errorObservable.addSource(observable, { parameter: 'groupExpression', key })
        if(removeObservable) {
          result.errorObservable.addSource(removeObservable, { parameter: 'removeExpression', key })
        }
      })

      return result

      function unpackGroups(currentValue) {
        return (currentValue && currentValue.map(unpackGroup)) || []
      }

      function unpackGroup(groupState) {
        const serializationInfo = {
          frames: groupState.state.map((x, i) => ({
            state: x,
            definition: groupExpressionDefinition && [
              // the base groupBy stream adds a frame so if there is no groupExpression, the currentValue is still preserved
              // add in an extra frame into the definition here to allow for this
              // use this frame to inject the "key" parameter closure
              groupExpressionParameters.length > 1
                ? serializeDefinition({ executionContext: { closures: {
                    [groupExpressionParameters[1]]: groupState.key
                  } } })
                : [],
              ...groupExpressionDefinition
            ][i]
          }))
        }

        const source = subject({ parent: inputObservable })
        const groupObservable = deserialize(
          operators,
          source, 
          serializationInfo
        )
        groupObservable.key = groupState.key
        
        const removeObservable = removeExpression && extendedRemoveExpression(source)

        return {
          observable: groupObservable,
          removeObservable,
          publish: source.publish,
          key: groupState.key,
          disconnect: () => result.errorObservable.removeSource(groupObservable)
        }
      }

      // we need to extract the group observables depending on whether we are in groupBy or hashBy mode
      // the logic here should be refactored out into "parent" modules
      function packGroups(currentValue) {
        return currentValue instanceof Array 
          ? currentValue.map(packGroup)
          : Object.keys(currentValue).map(key => packGroup(currentValue[key]))
      }

      function packGroup(observable) {
        return { 
          state: serialize(observable, true).frames,
          key: observable.key
        }
      }

      // this is not quite complete - serialization fails for some currently unknown reason
      function resolveGroupExpression(expression) {
        switch(typeof expression) {
          case 'function': return expression
          case 'string': return o => o.construct(expression)
          case 'object': return o => o.assign(expression)
          case 'undefined': return
          default: throw new Error('groupExpression must be undefined, an expression, hash of expressions or vocabulary name')
        }
      }
    }
  }
})