const { map } = require('../../streams').streams
const pure = require('../core/pure')

module.exports = pure(
  'stream',
  'map', 
  'stream',
  map, 
  expression => [expression]
)
