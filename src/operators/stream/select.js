const { map } = require('../../streams').streams
const pure = require('../core/pure')

module.exports = pure(
  'stream',
  'select', 
  'stream',
  map, 
  (...args) => [
    x => args.length === 1
      ? x[args[0]]
      : args.reduce(
          (result, property) => ({ ...result, [property]: x[property] }),
          {}
        )
  ]
)
