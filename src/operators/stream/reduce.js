const { reduce } = require('../../streams').streams
const pure = require('../core/pure')

module.exports = pure(
  'stream',
  'reduce', 
  'stream',
  reduce, 
  (reducer, initialValue) => {
    return [(accumulator, nextValue) => reducer(accumulator, nextValue), initialValue]
  }
)
