const { reduce } = require('../../streams').streams
const pure = require('../core/pure')
const { evaluateExpression } = require('../../utilities')

module.exports = pure(
  'stream',
  'accumulate', 
  'stream',
  reduce, 
  expression => [(accumulator, nextValue) => ({ ...accumulator, ...(evaluateExpression(expression, nextValue)) })],
  {}
)
