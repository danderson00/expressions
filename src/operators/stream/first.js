const { first } = require('../../streams').streams

// this ended up being really nasty
module.exports = {
  type: 'stream',
  identifier: 'first',
  component: inputObservable => {
    return {
      create: () => create(),
      unpack: ({ state }) => create(state)
    }

    function create(serializedState) {
      let valueSet = !!(serializedState && serializedState.valueSet)

      if(!valueSet) {
        const subscription = inputObservable.subscribe(() => {
          valueSet = true
          subscription.unsubscribe()
        })
      }

      return first(
        inputObservable,
        {
          initialValue: serializedState && serializedState.currentValue,
          getState: state => ({
            ...state,
            valueSet
          })
        },
        valueSet
      )
    }
  }
}
