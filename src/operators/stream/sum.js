const { reduce } = require('../../streams').streams
const pure = require('../core/pure')
const { evaluateExpression } = require('../../utilities')

module.exports = pure(
  'stream',
  'sum', 
  'stream',
  reduce, 
  expression => [(sum, nextValue) => sum += evaluateExpression(expression, nextValue)],
  0
)
