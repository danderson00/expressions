// pure operators are those that consist of "pure" functions
// i.e. functions that have no side effects and do not rely on any
// state other than the current value of the observable
// they can be deserialized from just the current value

module.exports = (type, identifier, returns, stream, createArgumentResolver, initialValue) => {
  return {
    type, 
    identifier, 
    component: inputObservable => {
      return {
        create: (...args) => {
          return create(args, initialValue)
        },
        unpack: ({ definition, state }) => create(
          definition.createArguments, 
          state && state.currentValue || initialValue
        )
      }

      function create(createArguments, initialValue) {
        return stream(
          inputObservable, 
          {
            initialValue,
            definition: {
              type,
              identifier,
              returns,
              createArguments
            }
          },
          ...createArgumentResolver(...createArguments)
        )
      }
    }
  }
}
