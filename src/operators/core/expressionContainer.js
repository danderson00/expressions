const observable = require('../../observable')
const { extractDefinition, serialize, unzipObservable, zipObservable, getFunctionParameters } = require('../../serialization')

// TODO: DEPRECATE expressionContainer
// this should be ripped out in favour of simply attaching the expression to the source...
// serialization should just work, and it's pretty much what it does anyway...

module.exports = {
  create: (operators, parent, expression, args = [], definition) => {
    const closures = getFunctionParameters(expression).slice(1).reduce(
      (closures, parameterName, index) => ({ ...closures, [parameterName]: args[index] }),
      {}
    )

    const innerObservable = operators.createExpression(
      parent,
      { executionContext: { closures } }
    )

    const expressionResult = expression(innerObservable, ...args)

    return createObservable(operators, parent, expressionResult, definition, args)
  },
  unpack: (operators, parent, info, expression) => {
    const innerObservable = operators.createExpression(
      parent,
      { executionContext: info.definition.executionContext }
    )

    const expressionResult = zipObservable(
      operators, 
      innerObservable,
      info.definition.frames || (expression && extractDefinition(operators, expression, { parameters: info.definition.parameters })),
      info.state && info.state.frames
    )
    
    return createObservable(operators, parent, expressionResult, info.definition)
  }
}

function createObservable(operators, parent, expressionResult, definition, parameters) {
  const expressionDefinition = unzipObservable(expressionResult).definition // TODO: just get definitions instead of using unzipObservable
  const expressionObservable = observable(
    expressionResult.subscribe,
    {
      parent,
      initialValue: expressionResult(),
      definition: {
        ...definition,
        frames: expressionDefinition,
        parameters: parameters || definition.parameters
      },
      getState: state => ({
        ...state,
        frames: serialize(expressionResult, true).frames
      })
    }
  )
  expressionObservable.errorObservable.addSource(expressionResult)
  return operators.extendObservable(expressionObservable)
}