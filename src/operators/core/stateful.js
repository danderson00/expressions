// stateful operators are similar to pure operators but can include extra state
// they are currently restricted to a single construction argument, an expression

module.exports = (type, identifier, returns, stream, resolver, userInitialState) => {
  return {
    type, identifier,
    component: inputObservable => {
      return {
        create: (...args) => create(args, userInitialState),
        unpack: ({ definition, state }) => create(definition.createArguments, state || userInitialState)
      }

      function create(createArguments, initialState) {
        let state = initialState
        const userExecutor = resolver(...createArguments)
        const streamExecutor = (...args) => {
          const result = userExecutor(state, ...args)
          state = { ...state, ...result.state }
          return result.value
        }

        return stream(
          inputObservable,
          {
            initialValue: initialState && initialState.currentValue,
            definition: {
              type,
              identifier,
              returns,
              createArguments
            },
            getState: currentState => ({ ...currentState, ...state })
          },
          streamExecutor
        )
      }
    }
  }
}
