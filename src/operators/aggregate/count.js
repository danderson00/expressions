const { map } = require('../../streams').streams
const pure = require('../core/pure')
const { unwrap } = require('../../utilities')

module.exports = pure(
  'aggregate',
  'count', 
  'stream',
  map, 
  () => [inputValue => unwrap(inputValue).length],
  0
)
