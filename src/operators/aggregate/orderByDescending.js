const { map } = require('../../streams').streams
const pure = require('../core/pure')
const { unwrap } = require('../../utilities')

module.exports = pure(
  'aggregate',
  'orderByDescending', 
  'aggregate',
  map, 
  expression => [inputValue => {
    return unwrap(inputValue).sort((a, b) =>
      typeof expression === 'string'
        ? b[expression] - a[expression]
        : expression(b) - expression(a)
    )
  }],
  []
)
