const { map } = require('../../streams').streams
const pure = require('../core/pure')

// highly inefficient as far as serialized payload size goes...

module.exports = pure(
  'aggregate',
  'asHash', 
  'stream',
  map, 
  () => [inputValue => inputValue.reduce(
    (result, value) => ({ ...result, [value.key]: value }),
    {}
  )],
  {}
)
