const { map } = require('../../streams').streams
const pure = require('../core/pure')
const { unwrap, evaluateExpression } = require('../../utilities')

module.exports = pure(
  'aggregate',
  'sum',
  'stream',
  map,
  expression => [inputValue => unwrap(inputValue).reduce(
    (sum, current) => sum + evaluateExpression(expression, current),
    0
  )],
  0
)
