const { map } = require('../../streams').streams
const pure = require('../core/pure')
const { unwrap } = require('../../utilities')

module.exports = pure(
  'aggregate',
  'mapAll',
  'stream',
  map, 
  expression => [inputValue => expression(unwrap(inputValue))]
)
