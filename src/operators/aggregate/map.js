const { map } = require('../../streams').streams
const pure = require('../core/pure')
const { unwrap } = require('../../utilities')

module.exports = pure(
  'aggregate',
  'map',
  'aggregate',
  map,
  expression => [inputValue => unwrap(inputValue).map(expression)],
  []
)
