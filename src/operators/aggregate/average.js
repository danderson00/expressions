const { map } = require('../../streams').streams
const pure = require('../core/pure')
const { unwrap, evaluateExpression } = require('../../utilities')

module.exports = pure(
  'aggregate',
  'average', 
  'stream',
  map, 
  expression => [inputValue => unwrap(inputValue).reduce(
    (total, current) => total + evaluateExpression(expression, current),
    0
  ) / inputValue.length],
  0
)
