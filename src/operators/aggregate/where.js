const { map } = require('../../streams').streams
const pure = require('../core/pure')
const { unwrap } = require('../../utilities')

module.exports = name => pure(
  'aggregate',
  name,
  'aggregate',
  map, 
  expression => [inputValue => unwrap(inputValue).filter(expression)],
  []
)
