const { map } = require('../../streams').streams
const pure = require('../core/pure')
const { unwrap } = require('../../utilities')

module.exports = pure(
  'aggregate',
  'orderBy', 
  'aggregate',
  map, 
  expression => [inputValue => {
    return unwrap(inputValue).sort((a, b) =>
      typeof expression === 'string'
        ? a[expression] - b[expression]
        : expression(a) - expression(b)
    )
  }],
  []
)
