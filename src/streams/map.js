const { operatorContainer } = require('../observable')

module.exports = (parent, options, expression) => {
  return operatorContainer(
    parent,
    (publish, registerSubscription, o) => {
      registerSubscription(parent.subscribe(
        inputValue => publish(expression(inputValue)),
        o.errorObservable.report
      ))
    },
    options
  )
}