const observable = require('../observable')
const { evaluateExpression, unwrap, isObservable } = require('../utilities')

module.exports = (parent, options = {}, expression, groupByExpression, removeExpression) => {
  const { operatorContainer } = observable
  let groups = options && options.initialValue || []
  let currentlyEvaluating = false
  
  return operatorContainer(
    parent,
    (publish, registerSubscription, o) => {
      registerSubscription(parent.subscribe(inputValue => {
        currentlyEvaluating = true
        const key = evaluateExpression(expression, inputValue)

        if(key !== undefined || options.includeUndefinedGroup) {
          let group = groups.find(x => x.key === key)

          if (!group) {
            group = createGroup(key)
            groups = groups.concat(group)
          }

          group.publish(inputValue)

          if (removeExpression) {
            const removedGroups = groups.filter(group => !!unwrap(group.removeObservable))
            removedGroups.forEach(group => group.disconnect())
            groups = groups.filter(group => !removedGroups.includes(group))
          }

          // publishing groups shouldn't strictly be necessary except when a new group is created
          // but is required in order to make subsequent aggregate operators work
          publish(transformGroups(options.hashBy, groups))
        }
        
        function createGroup(key) {
          // we need to inject an additional frame over the innerGroupObservable so that if there is no
          // groupExpression, the currentValue is still added to the serialized state
          const innerGroupObservable = observable.subject({ parent })
          const groupObservable = observable.proxy(innerGroupObservable)
          groupObservable.key = key

          const resultObservable = groupByExpression
            ? groupByExpression(groupObservable, key)
            : groupObservable
          
          if(!isObservable(resultObservable)) {
            throw new Error("groupExpression must return an observable")
          }

          resultObservable.key = key

          const disconnect = () => {
            o.errorObservable.removeSource(resultObservable)
            resultSubscription.unsubscribe()
          }

          groupObservable.disconnect = () => {
            disconnect()
            groups = groups.filter(group => group.observable !== resultObservable)
            publish(transformGroups(options.hashBy, groups))
          }
          resultObservable.disconnect = groupObservable.disconnect

          const removeObservable = removeExpression && removeExpression(
            observable.proxy(innerGroupObservable)
          )

          o.errorObservable.addSource(resultObservable, { parameter: 'groupExpression', key })
          if(removeObservable) {
            o.errorObservable.addSource(removeObservable, { parameter: 'removeExpression', key })
          }

          // if any result observables publish asynchronously, e.g. in a scope join,
          // we need to pulse the whole set of groups again. nasty but will do for now...
          // it should be fairly safe to not clean up this subscription
          // any result observables should go out of scope at the same time as this groupBy
          const resultSubscription = resultObservable.subscribe(() => {
            if(!currentlyEvaluating) {
              publish(transformGroups(options.hashBy, groups))
            }
          }, o.errorObservable.report)

          return {
            observable: resultObservable, 
            removeObservable,
            publish: innerGroupObservable.publish,
            key,
            disconnect
          }
        }

        currentlyEvaluating = false
      }, o.errorObservable.report))
    },
    { ...options, initialValue: transformGroups(options.hashBy, groups) }
  )
}

const transformGroups = (hashBy, groups) => (
  hashBy
    ? groups.reduce((result, group) => ({ ...result, [group.key]: group.observable }), {})
    : groups.map(x => x.observable)
)
