const { operatorContainer } = require('../observable')

module.exports = (parent, options, valueSet) => operatorContainer(
  parent,
  (publish, registerSubscription, o) => {
    if(!valueSet) {
      const subscription = registerSubscription(parent.subscribe(
        inputValue => {
          publish(inputValue)
          subscription.unsubscribe()
        },
        o.errorObservable.report
      ))
    }
  },
  options
)
