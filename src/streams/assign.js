const { operatorContainer } = require('../observable')
const { unwrap, deepEqual } = require('../utilities')

module.exports = (parent, options, observables) => {
  const calculateValue = () => Object.keys(observables).reduce(
    (result, key) => key.startsWith('...')
      ? { ...result, ...unwrap(observables[key]) }
      : { ...result, [key]: unwrap(observables[key]) },
    {}
  )

  let currentValue = calculateValue()

  return operatorContainer(
    parent,
    (publish, registerSubscription, o) => {
      const pulse = () => {
        const newValue = calculateValue()
        // this could be expensive in the long run if significant time is spent
        // in deepEqual, consider debouncing instead / as well
        // this will make the resolution of values asynchronous - probably not
        // an issue for real life scenarios but makes testing a bitch
        if(!deepEqual(currentValue, newValue)) {
          publish(newValue)
          currentValue = newValue
        }
      }

      // TODO: handle this case here and in compose
      // Object.keys(observables).forEach(key => {
      //   if(!isObservable(observables[key])) throw new Error(`key ${key} was not an observable`)
      // })

      Object.keys(observables).forEach(key => o.errorObservable.addSource(observables[key], { key }))

      // TODO: horribly inefficient
      Object.values(observables).forEach(o => registerSubscription(o.subscribe(pulse)))
      registerSubscription(parent.subscribe(pulse))
    },
    { initialValue: currentValue, ...options }
  )
}