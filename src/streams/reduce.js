const { operatorContainer } = require('../observable')

module.exports = (parent, options, expression, initialValue) => {
  let accumulator = (options && options.initialValue !== undefined) ? options.initialValue : initialValue

  return operatorContainer(
    parent,
    (publish, registerSubscription, o) => {
      registerSubscription(parent.subscribe(
        inputValue => {
          accumulator = expression(accumulator, inputValue)
          publish(accumulator)
        },
        o.errorObservable.report
      ))
    },
    { ...options, initialValue: accumulator }
  )
}