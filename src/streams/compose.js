const { operatorContainer } = require('../observable')
const { unwrap, deepEqual } = require('../utilities')

module.exports = (parent, options, observables, aggregator) => {
  const calculateValue = () => aggregator.apply(null, unwrap(observables))
  let currentValue = calculateValue()

  return operatorContainer(
    parent,
    (publish, registerSubscription, o) => {
      const pulse = () => {
        const newValue = calculateValue()
        // this could be expensive in the long run if significant time is spent
        // in deepEqual, consider debouncing instead / as well
        // this will make the resolution of values asynchronous - probably not
        // an issue for real life scenarios but makes testing a bitch
        if(!deepEqual(currentValue, newValue)) {
          publish(newValue)
          currentValue = newValue
        }
      }

      observables.forEach((x, i) => o.errorObservable.addSource(x, { parameter: i }))

      // TODO: horribly inefficient
      observables.forEach(o => registerSubscription(o.subscribe(pulse, o.errorObservable.report)))
      registerSubscription(parent.subscribe(pulse, o.errorObservable.report))
    },
    { initialValue: currentValue, ...options }
  )
}