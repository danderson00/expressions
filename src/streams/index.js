// streams contain the core functionality to extend observables
// their sole responsibility is to implement map / reduce / etc
// specific serialization mechanics are implemented in each component
// all streams have the signature (parent, options, ...args)
// args are generally passed on from the create function of a component
module.exports = {
  streams: {
    assign: require('./assign'),
    compose: require('./compose'),
    filter: require('./filter'),
    first: require('./first'),
    groupBy: require('./groupBy'),
    map: require('./map'),
    reduce: require('./reduce')
  }
}