const { operatorContainer } = require('../observable')

module.exports = (parent, options, expression) => {
  return operatorContainer(
    parent,
    (publish, registerSubscription, o) => {
      registerSubscription(parent.subscribe(
        inputValue => {
          switch(expression && typeof expression) {
            case 'function':
              if(expression(inputValue)) {
                publish(inputValue)
              }
              break;
            case 'object':
              if(Object.keys(expression).every(key => inputValue[key] === expression[key])) {
                publish(inputValue)
              }
              break;
            default:
              publish(inputValue)
          }
        },
        o.errorObservable.report
      ))
    },
    options
  )
}