const observable = require('../observable')
const { serialize } = require('./observable')

module.exports = {
  extractDefinition: (repository, streamExpression, options = {}) => {
    const { parameters, ...observableOptions } = options

    if(!repository || typeof repository !== 'object') {
      throw new Error(`Please use the extractDefinition function exposed by a @x/expressions instance rather than requiring this module directly`)
    }

    if(!streamExpression || typeof streamExpression !== 'function') {
      throw new Error(`You must pass an expression, e.g. o => o.where(x => x.type === 'example')`)
    }

    const inputObservable = repository.createExpression(observable(), observableOptions)

    let resultObservable

    try {
      resultObservable = streamExpression.apply(null, [inputObservable, ...(parameters || [])])
    } catch(error) {
      // TODO: use VError
      throw new Error(`There was an error executing your expression: ${error.message}`)
    }

    return serialize(resultObservable).frames.map(frame => frame.definition)
  },

  tryExtractDefinition: (repository, streamExpression, options) => {
    // sometimes we write expressions we know won't serialize
    try {
      return api.extractDefinition(repository, streamExpression, options)
    } catch(error) { }
  },

  extractReturnType: (repository, streamExpression, options) => {
    const definition = module.exports.extractDefinition(repository, streamExpression, options)
    return definition.length > 0
      ? definition[definition.length - 1].find(x => x.key === 'returns').value
      : 'stream'
  }
}