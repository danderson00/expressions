// adapted from https://www.geeksforgeeks.org/how-to-get-the-javascript-function-parameter-names-values-dynamically/
module.exports = {
  getFunctionParameters(target) {
    const parameterString = target.toString()
      .replace(/\/\*[\s\S]*?\*\//g, '')   // remove comments of the form /* ... */
      .replace(/\/\/(.)*/g, '')           // remove comments of the form //
      .replace(/({|=>)[\s\S]*/, '')       // remove all after the start of the function body
      .replace(/\n/g, '')                 // remove any carriage returns
      .trim()

    return parameterString
      .substring(
        parameterString.indexOf("(") + 1,             // start after first '('
        parameterString.lastIndexOf(')') === -1       // end just before last ')'
          ? parameterString.length
          : parameterString.lastIndexOf(')')
      )
      .split(",")
      .map(x => x.replace(/=[\s\S]*/g, '').trim())    // remove default value
      .filter(x => x.length > 0)
  }
}
