module.exports = {
  getExecutionContext: observable => {
    // recurse up through parent observables, merging closures
    const definition = observable.getSerializationInfo && observable.getSerializationInfo().definition || {}
    return {
      closures: {
        ...(observable.parent && module.exports.getExecutionContext(observable.parent).closures),
        ...(definition.executionContext && definition.executionContext.closures)
      }
    }
  }
}

// executionContext is used as part of deserialization to support things
// like closures. Ideally, all serialization related aspects should be
// handled by individual operators, but closures are a problem common
// to almost all operators, and there is no way with the current API
// to inject execution context at the individual component level, e.g. 
//   .where(x => x.category === props.category), { category: props.category })
// so some basic support is in observable.js to set some simple serializationInfo
// from an option

// the only current use case for closures is for dynamically evaluated
// expressions (e.g. search), and since executionContext cascades down to
// subsequent expression operators, handling it at the "root" observable
// seems like a reasonable approach for now, e.g.
//   props.evaluate(
//     o => o.where(x => x.category === props.category),
//     { category: props.category }
//   )
// this means the "root" observable and all operators below it serve as
// a container for execution context, a scope if you will.
// additional execution context can be injected by using a proxy, e.g.
//   expressions.proxy(observable, { executionContext: { closures: { a: 1 } } })
// making the proxy and operators below it a "child scope"

// the abstraction is not very crisp - longer term, a serialized observable
// should have a top level context container that contains the context and 
// stack frames rather than just an array of stack frames (current status). 
// this way, individual operators can have their own "child" context container