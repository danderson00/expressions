const { serialize, deserialize, clone } = require('./observable')
const { getFunctionParameters } = require('./functionParameters')
const { extractDefinition, tryExtractDefinition, extractReturnType } = require('./extractDefinition')
const frames = require('./frames')

const api = module.exports = {
  serialize,
  deserialize,
  clone,
  frames,
  getFunctionParameters,
  extractDefinition,
  tryExtractDefinition,
  extractReturnType,

  unzipObservable: observable => (
    frames.unzip(api.serialize(observable).frames)
  ),

  zipObservable: (repository, parent, definition, state, error, options) => (
    api.deserialize(repository, parent, { frames: frames.zip(definition, state), error }, options)
  )
}
