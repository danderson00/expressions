module.exports = {
  unzip: frames => (
    frames.reduce(
      (result, frame) => ({
        definition: [...result.definition, frame.definition],
        state: [...result.state, frame.state]
      }),
      { definition: [], state: [] }
    )
  ),
  zip: (definition, state) => {
    if(!definition) {
      throw new Error("No definition provided")
    }

    if(state && definition.length !== state.length) {
      throw new Error("Definition and state lengths do not match")
    }

    return definition.map((x, i) => ({
      definition: x,
      state: state && state[i]
    }))
  }
}
