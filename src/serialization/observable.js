const { serializeDefinition, deserializeDefinition } = require('./definition')
const { getExecutionContext } = require('./executionContext')
const observable = require('../observable')

const api = module.exports = {
  serialize: (inputObservable, stateOnly) => {
    return {
      frames: serializeFrame(inputObservable, stateOnly)
    }

    function serializeFrame(inputObservable, stateOnly, stack = []) {
      if(inputObservable && inputObservable.getSerializationInfo) {
        const info = inputObservable.getSerializationInfo()
        if(info.definition && info.definition.source) {
          // stop at a source observable and omit it from serialization
          // sources can't be serialized and it stops multiple source
          // observables from accumulating over multiple serializations
          return stack
        } else {
          return serializeFrame(
            inputObservable.parent,
            stateOnly,
            (info.definition && info.definition.skip)
              ? stack
              : [stateOnly ? info.state : serializeDefinition(info), ...stack]
          )
        }
      } else {
        return stack
      }
    }
  },

  deserialize: (repository, parent, info) => {
    const deserializeFrame = (parent, repository, info) => {
      return (info.definition && info.definition.identifier)
        ? repository.find(info.definition.type, info.definition.identifier).component(parent, repository).unpack(info)
        // should probably throw an exception here, or maybe just skip?
        : observable.proxy(parent, { initialValue: info.state && info.state.currentValue, definition: info.definition })
    }

    return info.frames.reduce(
      (target, frame, index) => {
        const info =  deserializeDefinition(frame, getExecutionContext(target))
        const result = deserializeFrame(target, repository, info)
        result.errorObservable.addContext({
          operator: info.definition && info.definition.identifier || 'unknown',
          position: index + 1
        })
        result.errorObservable.publish(info.error)
        return result
      },
      parent
    )
  },

  clone: (repository, parent, o) => api.deserialize(repository, parent, api.serialize(o))
}