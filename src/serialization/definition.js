// reaaally not sure if this is a good idea. the rabbit hole is deep on this one...

const api = module.exports = {
  serializeDefinition: info => ({ ...info, definition: info.definition && api.serialize(info.definition) }),
  deserializeDefinition: (info, executionContext) => ({ ...info, definition: info.definition && api.deserialize(info.definition, executionContext) }),

  serialize: source => source
    ? Object.keys(source).map(
        key => ({ ...serializeValue(source[key]), key })
      )
    : [],
  deserialize: (source, executionContext) => source.reduce(
    (target, property) => {
      target[property.key] = deserializeValue(property, executionContext)
      return target
    },
    {}
  )
}

function serializeValue(value) {
  switch(typeof value) {
    case 'function':
      return {
        type: 'function',
        value: value.toString()
      }
    case 'object':
      return (value && value.constructor === Array)
        ? {
            type: 'array',
            value: value.map(serializeValue)
          }
        : { value }
    default:
      return { value }
  }
}

function deserializeValue(value, executionContext = {}) {
  const closures = executionContext.closures || {}
  switch(value.type) {
    case 'function':
      return Function.constructor.apply(null, [
        ...(Object.keys(closures)),
        `return ${value.value}`
      ]).apply(null, Object.keys(closures).map(x => closures[x]))
    case 'array':
      return value.value.map(x => deserializeValue(x, executionContext))
    default:
      return value.value
  }
}