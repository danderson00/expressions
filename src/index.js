const observable = require('@x/observable')
const serializable = require('./observable')
const operators = require('./operators')
const repository = require('./repository')
const { serialize, deserialize, unzipObservable, zipObservable, extractDefinition, extractReturnType, clone } = require('./serialization')
const { unwrap, isObservable } = require('./utilities')

const construct = () => {
  const container = repository(operators)
  const createSources = base => ({
    subject: options => container.extendObservable(base.subject(options)),
    proxy: (inputObservable, options) => container.extendObservable(base.proxy(inputObservable, options)),
    fromEmitter: (emitter, ...eventNames) => container.extendObservable(base.fromEmitter(emitter, ...eventNames)),
    swappable: (inputObservable, options) => container.extendObservable(base.swappable(inputObservable, options)),
    merge: (...inputObservables) => container.extendObservable(base.merge(...inputObservables)),
    mergeHash: source => container.extendObservable(base.mergeHash(source)),
    mergeArray: (...inputObservables) => container.extendObservable(base.mergeArray(...inputObservables)),
  })

  return Object.assign(
    (inputObservable, options) => container.createExpression(inputObservable, options),
    {
      createExpression: (inputObservable, options) => container.createExpression(inputObservable, options),
      ...createSources(observable),
      serializable: createSources(serializable),
      addOperator: container.add,
      addVocabulary: container.addVocabulary,
      getVocabularyShallow: container.getVocabularyShallow,

      serialize,
      deserialize: (parent, info, options) => deserialize(container, parent, info, options),
      unzip: unzipObservable,
      zip: (parent, definition, state, error, options) => zipObservable(container, parent, definition, state, error, options),
      extractDefinition: (streamExpression, options) => extractDefinition(container, streamExpression, options),
      extractReturnType: (streamExpression, options) => extractReturnType(container, streamExpression, options),
      clone: (parent, o) => clone(container, parent, o),

      observable,
      unwrap,
      isObservable
    }
  )
}

module.exports = construct()
module.exports.construct = construct