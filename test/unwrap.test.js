const { unwrap } = require('../src/utilities')
const { subject } = require('../src')

const o = initialValue => subject({ initialValue })

test("unwrap evaluates observables", () => {
  expect(unwrap(o(2))).toBe(2)
  expect(unwrap(o({ p1: 1 }))).toEqual({ p1: 1 })
})

test("unwrap recursively evaluates observables", () => {
  expect(unwrap(o(o(o(2))))).toBe(2)
})

test("unwrap evaluates arrays of observables", () => {
  expect(unwrap([o(1), o(2), o(3)])).toEqual([1, 2, 3])
})

test("unwrap evaluates properties of objects", () => {
  expect(unwrap({
    p1: o(1),
    p2: o(2)
  })).toEqual({
    p1: 1,
    p2: 2
  })
})

test("unwrap preserves existing properties of objects", () => {
  const f = () => 1
  expect(unwrap({
    o: o(1),
    f: f,
    b: true,
    s: 'string',
    n: 2,
    a: [],
    o2: {}
  })).toEqual({
    o: 1,
    f: f,
    b: true,
    s: 'string',
    n: 2,
    a: [],
    o2: {}
  })
})

test("unwrap recurses comprehensively", () => {
  expect(unwrap(o({
    a: [
      o({
        p1: o(1),
        p2: [o(2)]
      }),
      o(2)
    ],
    b: [
      o(3),
      o('string')
    ]
  }))).toEqual({
    a: [
      {
        p1: 1,
        p2: [2]
      },
      2
    ],
    b: [
      3,
      'string'
    ]
  })
})