const reduce = require('../../src/streams/reduce')
const observable = require('../../src/observable')

test("reduce accumulates values", () => {
  let publish
  const handler = jest.fn()
  const o = observable(p => publish = p)
  reduce(o, null, (x, y) => x + y * 2, 0).subscribe(handler)
  publish(1)
  publish(2)
  publish(3)
  expect(handler.mock.calls.length).toEqual(3)
  expect(handler.mock.calls.map(call => call[0])).toEqual([2, 6, 12])
})