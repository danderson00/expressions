const groupBy = require('../../src/streams/groupBy')
const filter = require('../../src/streams/filter')
const observable = require('../../src/observable')

test("groupBy creates groups based on expression", () => {
  const source = observable.subject()
  const groups = groupBy(source, undefined, x => x % 2)
  source.publish(1)
  expect(groups().map(x => x())).toEqual([1])
  expect(groups().map(x => x.key)).toEqual([1])
  source.publish(2)
  expect(groups().map(x => x())).toEqual([1, 2])
  expect(groups().map(x => x.key)).toEqual([1, 0])
  source.publish(3)
  expect(groups().map(x => x())).toEqual([3, 2])
  expect(groups().map(x => x.key)).toEqual([1, 0])
})

test("groupBy creates groups based on property name", () => {
  const source = observable.subject()
  const groups = groupBy(source, undefined, 'category')
  source.publish({ category: 1 })
  expect(groups().map(x => x())).toEqual([{ category: 1 }])
  expect(groups().map(x => x.key)).toEqual([1])
  source.publish({ category: 2 })
  expect(groups().map(x => x())).toEqual([{ category: 1 }, { category: 2 }])
  expect(groups().map(x => x.key)).toEqual([1, 2])
  source.publish({ category: 3 })
  expect(groups().map(x => x())).toEqual([{ category: 1 }, { category: 2 }, { category: 3 }])
  expect(groups().map(x => x.key)).toEqual([1, 2, 3])
})

test("groupBy removes groups based on remove expression", () => {
  const source = observable.subject()
  const groups = groupBy(source, undefined, 'category', undefined, o => filter(o, undefined, x => x.remove))
  source.publish({ category: 1 })
  source.publish({ category: 2 })
  source.publish({ category: 1, remove: true })
  expect(groups().map(x => x())).toEqual([{ category: 2 }])
  expect(groups().map(x => x.key)).toEqual([2])
})

test("groupObservable.disconnect removes group", () => {
  const source = observable.subject()
  const groups = groupBy(source, undefined, 'category')
  source.publish({ category: 1 })
  source.publish({ category: 2 })

  groups()[0].disconnect()

  expect(groups().map(x => x())).toEqual([{ category: 2 }])
  expect(groups().map(x => x.key)).toEqual([2])
})

test("groupObservable.disconnect removes group when groupExpression is specified", () => {
  const source = observable.subject()
  const groups = groupBy(source, undefined, 'category', o => observable.proxy(o))
  source.publish({ category: 1 })
  source.publish({ category: 2 })

  groups()[0].disconnect()

  expect(groups().map(x => x())).toEqual([{ category: 2 }])
  expect(groups().map(x => x.key)).toEqual([2])
})

test("asynchronously published events cause observable to pulse", () => {
  const source = observable.subject()
  const groupObservable = observable.subject()
  const groups = groupBy(source, undefined, 'category', () => groupObservable)
  source.publish({ category: 1 })

  const spy = jest.fn()
  groups.subscribe(spy)
  groupObservable.publish({ })
  expect(spy.mock.calls.length).toBe(1)
})

test("synchronously published events cause observable to pulse only once", () => {
  const source = observable.subject()
  const groups = groupBy(source, undefined, 'category', o => o)
  source.publish({ category: 1 })

  const spy = jest.fn()
  groups.subscribe(spy)
  source.publish({ category: 2 })
  source.publish({ category: 1 })
  expect(spy.mock.calls.length).toBe(2)
})

test("undefined groups are excluded by default", () => {
  const source = observable.subject()
  const groups = groupBy(source, undefined, 'category')
  source.publish({ category: 1 })
  source.publish({})
  expect(groups().map(x => x())).toEqual([{ category: 1 }])
})

test("undefined groups are included if includeUndefinedGroup option is specified", () => {
  const source = observable.subject()
  const groups = groupBy(source, { includeUndefinedGroup: true }, 'category')
  source.publish({ category: 1 })
  source.publish({})
  expect(groups().map(x => x())).toEqual([{ category: 1 }, { category: undefined }])
})

test("key is passed as second parameter to groupExpression", () => {
  const source = observable.subject()
  const groups = groupBy(source, undefined, 'category', (o, key) => observable.subject({ initialValue: key }))
  source.publish({ category: 1 })
  source.publish({ category: 2 })
  expect(groups().map(x => x())).toEqual([1, 2])
})
