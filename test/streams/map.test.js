const map = require('../../src/streams/map')
const observable = require('../../src/observable')

test("map mutates each message in stream", () => {
  let publish
  const handler = jest.fn()
  const o = observable(p => publish = p)
  map(o, null, x => x * 2).subscribe(handler)
  publish(1)
  publish(2)
  publish(3)
  expect(handler.mock.calls.length).toEqual(3)
  expect(handler.mock.calls.map(call => call[0])).toEqual([2, 4, 6])
})