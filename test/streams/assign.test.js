const assign = require('../../src/streams/assign')
const observable = require('../../src/observable')

test("assign aggregates from several observables, spreading where requested", () => {
  const source = observable.subject()
  const o1 = observable.subject()
  const o2 = observable.subject()
  const c = assign(source, undefined, { o1, '...o2': o2 })
  o1.publish(1)
  o2.publish({ p1: 1 })
  expect(c()).toEqual({ o1: 1, p1: 1 })
  o1.publish(2)
  expect(c()).toEqual({ o1: 2, p1: 1 })
  o2.publish({ p2: 2 })
  expect(c()).toEqual({ o1: 2, p2: 2 })
})

test("assign sets initial value", () => {
  const source = observable.subject()
  const o1 = observable.subject()
  const o2 = observable.subject()
  const c = assign(source, undefined, { o1, '...o2': o2 })

  expect(c()).toEqual({ o1: undefined })
})

test("assign only publishes new value when output value changes", () => {
  const source = observable.subject()
  const o1 = observable.subject()
  const o2 = observable.subject()
  const c = assign(source, undefined, { o1, '...o2': o2 })
  const stub = jest.fn()
  c.subscribe(stub)
  o1.publish(1)
  o2.publish({ p1: 1 })
  expect(stub.mock.calls).toEqual([
    [{ o1: 1 }],
    [{ o1: 1, p1: 1 }]
  ])
  o1.publish(1)
  o2.publish({ p1: 1 })
  expect(stub.mock.calls.length).toBe(2)
  o1.publish(2)
  expect(stub.mock.calls).toEqual([
    [{ o1: 1 }],
    [{ o1: 1, p1: 1 }],
    [{ o1: 2, p1: 1 }]
  ])
})
