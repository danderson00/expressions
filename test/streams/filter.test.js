const filter = require('../../src/streams/filter')
const { subject } = require('../../src/observable')

test("filter removes non matching messages", () => {
  const handler = jest.fn()
  const o = subject()
  filter(o, null, x => x % 2 === 0).subscribe(handler)
  o.publish(1)
  o.publish(2)
  o.publish(3)
  expect(handler.mock.calls.length).toEqual(1)
  expect(handler.mock.calls[0][0]).toEqual(2)
})

test("filter can be passed a hash of values to match", () => {
  const handler = jest.fn()
  const o = subject()
  filter(o, null, { p1: 1, p2: 'test' }).subscribe(handler)
  o.publish({ p1: 1, p2: 'test', p3: 10 })
  o.publish({ p1: 2, p2: 'test', p3: 20 })
  o.publish({ p1: 1, p2: 'test2', p3: 30 })
  o.publish({ p1: 1, p2: 'test', p3: 40 })
  expect(handler.mock.calls.length).toEqual(2)
  expect(handler.mock.calls.map(x => x[0].p3)).toEqual([10, 40])
})