const first = require('../../src/streams/first')
const observable = require('../../src/observable')

test("first retains first published value", () => {
  let publish
  const handler = jest.fn()
  const o = observable(p => publish = p)
  first(o).subscribe(handler)
  publish(1)
  publish(2)
  publish(3)
  expect(handler.mock.calls).toEqual([[1]])
})