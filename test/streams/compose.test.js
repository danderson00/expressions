const compose = require('../../src/streams/compose')
const observable = require('../../src/observable')

test("compose allows aggregation from several observables", () => {
  const source = observable.subject()
  const o1 = observable.subject()
  const o2 = observable.subject()
  const c = compose(source, undefined, [o1, o2], (o1, o2) => o1 + o2)
  o1.publish(1)
  o2.publish(2)
  expect(c()).toBe(3)
  o1.publish(2)
  expect(c()).toBe(4)
  o2.publish(3)
  expect(c()).toBe(5)
})

test("compose sets initial value", () => {
  const source = observable.subject()
  const o1 = observable.subject()
  const o2 = observable.subject()
  const c = compose(source, undefined, [o1, o2], (o1, o2) => o1 + o2)

  expect(c()).toEqual(NaN)
})

test("compose only publishes new value when output value changes", () => {
  const source = observable.subject()
  const o1 = observable.subject()
  const o2 = observable.subject()
  const c = compose(source, undefined, [o1, o2], (o1, o2) => ({ ...o1, ...o2 }))
  const stub = jest.fn()
  c.subscribe(stub)
  o1.publish({ p1: 1 })
  o2.publish({ p2: 2 })
  expect(stub.mock.calls).toEqual([
    [{ p1: 1 }],
    [{ p1: 1, p2: 2 }]
  ])
  o1.publish({ p1: 1 })
  o2.publish({ p2: 2 })
  expect(stub.mock.calls.length).toBe(2)
  o1.publish({ p1: 2 })
  expect(stub.mock.calls).toEqual([
    [{ p1: 1 }],
    [{ p1: 1, p2: 2 }],
    [{ p1: 2, p2: 2 }]
  ])
})
