const { subject } = require('../../src')

test("groupBy returns observable extended with aggregate operations", () => {
  const observable = subject().groupBy(x => x % 2)
  expect(observable.groupBy).toBeUndefined()
  expect(observable.count).toBeInstanceOf(Function)
})