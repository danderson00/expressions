const repository = require('../../src/repository')
const operators = require('../../src/operators')

test("getVocabularyShallow returns default value and return type", () => {
  const container = repository(operators)

  container.addVocabulary({
    sumOfValues: o => o.sum(),
    oddsAndEvens: o => o.groupBy(x => x % 2),
    parameterised: (o, category) => o
  })

  expect(container.getVocabularyShallow()).toEqual([
    {
      name: 'sumOfValues',
      defaultValue: 0,
      parameters: [],
      returns: 'stream'
    },
    {
      name: 'oddsAndEvens',
      defaultValue: [],
      parameters: [],
      returns: 'aggregate'
    },
    {
      name: 'parameterised',
      defaultValue: undefined,
      parameters: ['category'],
      returns: 'stream'
    }
  ])
})