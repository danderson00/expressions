const { subject } = require('../src/observable')
const expressions = require('../src')
const { serialize, deserialize } = require('../src/serialization')
const operators = require('../src/repository')(require('../src/operators'))
const { unwrap } = require('../src/utilities')

test("groupBy with count", () => {
  const source = subject()
  const o = expressions.createExpression(source).groupBy(x => x.category).count()
  
  source.publish({ category: 1 })
  source.publish({ category: 2 })
  source.publish({})

  expect(o()).toEqual(2)

  const json = JSON.stringify(serialize(o))
  const d = deserialize(operators, source, JSON.parse(json))

  expect(d()).toEqual(2)
  source.publish({ category: 1 })
  expect(d()).toEqual(2)
  source.publish({ category: 3 })
  expect(d()).toEqual(3)
})

test("practical groupBy application", () => {
  const source = subject()
  const o = expressions.createExpression(source)
    .groupBy(
      x => x.category,
      o => o.groupBy(x => x.user).average(x => x.rating)
    )

  source.publish({ category: 'cat1', user: 'user1', rating: 1 })
  source.publish({ category: 'cat1', user: 'user2', rating: 2 })
  source.publish({ category: 'cat1', user: 'user3', rating: 3 })
  expect(unwrap(o)).toEqual([2])

  source.publish({ category: 'cat2', user: 'user1', rating: 2 })
  source.publish({ category: 'cat2', user: 'user2', rating: 4 })
  source.publish({ category: 'cat2', user: 'user3', rating: 6 })
  expect(unwrap(o)).toEqual([2, 4])

  source.publish({ category: 'cat3', user: 'user1', rating: 1 })
  source.publish({ category: 'cat3', user: 'user1', rating: 2 })
  expect(unwrap(o)).toEqual([2, 4, 2])

  const json = JSON.stringify(serialize(o))
  const d = deserialize(operators, source, JSON.parse(json))

  source.publish({ category: 'cat2', user: 'user3', rating: 9 })
  expect(unwrap(d)).toEqual([2, 5, 2])
})

test("mapping into key/value pairs", () => {
  const source = subject()
  const o = expressions.createExpression(source)
    .groupBy(
      'category',
      o => o.compose(
        o => o.groupBy('user').average(x => x.rating),
        rating => ({ category: o.key, rating: rating })
      )
    )

  source.publish({ category: 'cat1', user: 'user1', rating: 1 })
  source.publish({ category: 'cat2', user: 'user2', rating: 4 })

  expect(unwrap(o)).toEqual([
    { category: 'cat1', rating: 1 }, 
    { category: 'cat2', rating: 4 }
  ])
})


test("groupBy with asHash", () => {
  const source = subject()
  const o = expressions.createExpression(source)
    .groupBy(
      'category',
      o => o.compose(
        o => o.groupBy('user').average(x => x.rating),
        rating => ({ category: o.key, rating: rating })
      )
    )
    .asHash()

  source.publish({ category: 'cat1', user: 'user1', rating: 1 })
  source.publish({ category: 'cat2', user: 'user2', rating: 4 })

  expect(unwrap(o)).toEqual({
    cat1: { category: 'cat1', rating: 1 }, 
    cat2: { category: 'cat2', rating: 4 }
  })
})

test("vocabulary returns observable extended with correct type", () => {
  const source = subject()
  expressions.addVocabulary({
    name: 'groups', 
    expression: o => o.groupBy(
      'category',
      o => o.compose(
        o => o.groupBy('user').average(x => x.rating),
        rating => ({ category: o.key, rating: rating })
      )
    )
  })
  const o = expressions.createExpression(source).groups().asHash()

  source.publish({ category: 'cat1', user: 'user1', rating: 1 })
  source.publish({ category: 'cat2', user: 'user2', rating: 4 })

  expect(unwrap(o)).toEqual({
    cat1: { category: 'cat1', rating: 1 }, 
    cat2: { category: 'cat2', rating: 4 }
  })
})

test("filtered groupBy with compose", () => {
  const source = subject()
  const o = expressions.createExpression(source)
    .groupBy(
      'category',
      o => o.compose(
        o => o.groupBy('user').average(x => x.rating),
        rating => ({ category: o.key, rating: rating })
      )
    )
    .where(x => x.rating > 3)

  source.publish({ category: 'cat1', user: 'user1', rating: 1 })
  source.publish({ category: 'cat2', user: 'user2', rating: 4 })
  source.publish({ category: 'cat2', user: 'user1', rating: 8 })
  source.publish({ category: 'cat3', user: 'user2', rating: 5 })

  expect(unwrap(o)).toEqual([
    { category: 'cat2', rating: 6 },
    { category: 'cat3', rating: 5 }
  ])
})

test("ordered groupBy with compose", () => {
  const source = subject()
  const o = expressions.createExpression(source)
    .groupBy(
      'category',
      o => o.compose(
        o => o.groupBy('user').average(x => x.rating),
        rating => ({ category: o.key, rating: rating })
      )
    )
    .orderBy('rating')

  source.publish({ category: 'cat1', user: 'user1', rating: 1 })
  source.publish({ category: 'cat2', user: 'user2', rating: 4 })
  source.publish({ category: 'cat2', user: 'user1', rating: 8 })
  source.publish({ category: 'cat3', user: 'user2', rating: 5 })

  expect(unwrap(o)).toEqual([
    { category: 'cat1', rating: 1 },
    { category: 'cat3', rating: 5 },
    { category: 'cat2', rating: 6 }
  ])
})

test("groupBy with groupExpression using key parameter", () => {
  const source = subject()
  const o = expressions.createExpression(source).groupBy('category',
    (o, keyParameter) => o.compose(
      o => o.count(),
      count => ({ key: keyParameter, count })
    )
  )

  source.publish({ category: 'cat1' })
  expect(unwrap(o)).toEqual([{ key: 'cat1', count: 1 }])
  source.publish({ category: 'cat1' })
  source.publish({ category: 'cat2' })
  expect(unwrap(o)).toEqual([{ key: 'cat1', count: 2 }, { key: 'cat2', count: 1 }])

  const json = JSON.stringify(serialize(o))
  const d = deserialize(operators, source, JSON.parse(json))

  expect(unwrap(d)).toEqual([{ key: 'cat1', count: 2 }, { key: 'cat2', count: 1 }])
  source.publish({ category: 'cat1' })
  source.publish({ category: 'cat3' })
  expect(unwrap(d)).toEqual([{ key: 'cat1', count: 3 }, { key: 'cat2', count: 1 }, { key: 'cat3', count: 1 }])
})
