const { construct } = require('../src')

const error = new Error('test')
const t = () => { throw new Error('test') }

const setup = (expression, serializeObservable, vocabulary) => {
  const { subject, serialize, deserialize, addVocabulary } = construct()

  if(vocabulary) {
    addVocabulary(vocabulary)
  }

  let source = subject()
  let result = expression(source)

  if(serializeObservable) {
    source = subject()
    result = deserialize(source, serialize(result))
  }

  const spy = jest.fn()
  result.errorObservable.subscribe(spy)
  return { publish: source.publish, mock: spy.mock, source, o: result, serialize, deserialize }
}

describe("stream operators", () => {
  // the following are not testable:
  // - accept no arguments - all, count, first
  // - multiple string arguments - select, topic
  // - integer arguments - take, takeLast

  describe.each([['', false], [' (serialized)', true]])(
    "simple expression test%s", (_, serialized) => {
      test.each(['accumulate', 'average', 'groupBy', 'map', 'reduce', 'sum', 'where'])(
        "%s", operator => {
          const { publish, mock } = setup(
            o => o[operator](t),
            serialized
          )
          publish({})
          expect(mock.calls).toEqual([[{ frames: [{ operator, position: 1 }], error }]])
        }
      )
    }
  )

  test.each([['', false], [' (serialized)', true]])(
    "assign%s", (_, serialized) => {
      const { publish, mock } = setup(
        o => o.assign({
          p1: o => o.map(() => { throw new Error('test') })
        }),
        serialized
      )
      publish({})
      expect(mock.calls).toEqual([[{
        frames: [
          { operator: 'assign', position: 1, key: 'p1' },
          { operator: 'map', position: 1 }
        ],
        error
      }]])
    }
  )

  describe.each([['', false], [' (serialized)', true]])(
    "compose%s", (_, serialized) => {
      test("error in expressions", () => {
        const { publish, mock } = setup(
          o => o.compose(
            o => o.map(() => { throw new Error('test') }),
            o1 => o1
          ),
          serialized
        )
        publish({})
        expect(mock.calls).toEqual([[{
          frames: [
            { operator: 'compose', position: 1, parameter: 0 },
            { operator: 'map', position: 1 }
          ],
          error
        }]])
      }
    )

    test.skip("error in aggregator", () => {
      const { publish, mock } = setup(
        o => o.compose(
          o => o,
          value => { if(value === 1) throw new Error('test') }
        ),
        serialized
      )
      publish(1)
      expect(mock.calls).toEqual([
        [{ frames: [
          { operator: 'compose', position: 1 }
        ], error }]
      ])
    })
  })

  describe.each([['', false], [' (serialized)', true]])(
    "groupBy%s", (_, serialized) => {
      test("error in groupExpression", () => {
        const { publish, mock } = setup(
          o => o.groupBy('category', o => o.map(() => { throw new Error('test') })),
          serialized
        )
        publish({ category: 1 })
        expect(mock.calls).toEqual([[{
          frames: [
            { operator: 'groupBy', position: 1, parameter: 'groupExpression', key: 1 },
            { operator: 'map', position: 1 }
          ],
          error
        }]])
      })

      test("error in removeExpression", () => {
        const { publish, mock } = setup(
          o => o.groupBy('category', o => o, o => o.map(() => { throw new Error('test') })),
          serialized
        )
        publish({ category: 1 })
        expect(mock.calls).toEqual([[{
          frames: [
            { operator: 'groupBy', position: 1, parameter: 'removeExpression', key: 1 },
            { operator: 'map', position: 1 }
          ],
          error
        }]])
      })
    }
  )
})

describe("aggregate operators", () => {
  // the following are not testable:
  // - accept no arguments - asHash, count

  describe.each([['', false], [' (serialized)', true]])(
    "simple expression test%s", (_, serialized) => {
      test.each(['average', 'map', 'mapAll', 'sum', 'where'])(
        "%s", operator => {
          const { publish, mock } = setup(
            o => o.groupBy('category')[operator](t),
            serialized
          )
          publish({ category: 1 })
          expect(mock.calls).toEqual([[{ frames: [
            { operator, position: 2 }
          ], error }]])
        }
      )
    }
  )

  describe.each([['', false], [' (serialized)', true]])(
    "orderBy expression test%s", (_, serialized) => {
      test.each(['orderBy', 'orderByDescending'])(
        "%s", operator => {
          const { publish, mock } = setup(
            o => o.groupBy('category')[operator](t),
            serialized
          )
          publish({ category: 1 })
          publish({ category: 2 })
          expect(mock.calls).toEqual([[{
            frames: [{ operator, position: 2 }],
            error
          }]])
        }
      )
    }
  )
})

test.each([['', false], [' (serialized)', true]])(
  "vocabulary%s", (_, serialized) => {
    const { publish, mock } = setup(
      o => o.mapThrow(),
      serialized,
      { mapThrow: o => o.map(() => { throw new Error('test') }) }
    )
    publish({ category: 1 })
    expect(mock.calls).toEqual([[{
      frames: [
        { operator: 'mapThrow', position: 1 },
        { operator: 'map', position: 1 }
      ],
      error
    }]])
  }
)

describe.each([['', false], [' (serialized)', true]])(
  "nested%s", (_, serialized) => {
    test("groupBy", () => {
      const { publish, mock } = setup(
        o => o.groupBy('category1',
          o => o.groupBy('category2',
            o => o.map(() => { throw new Error('test') })
          )
        ),
        serialized
      )
      publish({ category1: 1, category2: 2 })
      expect(mock.calls).toEqual([[{
        frames: [
          { parameter: 'groupExpression', key: 1, operator: 'groupBy', position: 1 },
          { parameter: 'groupExpression', key: 2, operator: 'groupBy', position: 1 },
          { operator: 'map', position: 1 },
        ],
        error
      }]])
    })

    test("multiple", () => {
      const { publish, mock } = setup(
        o => o.groupBy('category',
          o => o.assign({
            p1: o => o.compose(
              o => o.map(() => { throw new Error('test') }),
              o1 => o1
            )
          })
        ),
        serialized
      )
      publish({ category: 1 })
      expect(mock.calls).toEqual([[{
        frames: [
          { operator: 'groupBy', position: 1, parameter: 'groupExpression', key: 1 },
          { operator: 'assign', position: 1, key: 'p1' },
          { operator: 'compose', position: 1, parameter: 0 },
          { operator: 'map', position: 1 }
        ],
        error
      }]])
    })

    test("constructed vocabulary containing assign", () => {
      const { publish, mock } = setup(
        o => o.construct('contractor'),
        serialized,
        {
          contractor: o => o.assign({
            contractorId: o => o.topic('contractor').select('contractorId'),
            '...details': o => o.topic('contractor').accumulate('contractor'),
            averageRating: o => o.map(() => asd)
          })
        }
      )
      publish({})
      expect(mock.calls).toEqual([[{
        "error": new ReferenceError('asd is not defined'),
        "frames": [
          {
            "operator": "contractor",
            "position": 1
          },
          {
            "key": "averageRating",
            "operator": "assign",
            "position": 1
          },
          {
            "operator": "map",
            "position": 1
          }
        ]
      }]])
    })
  }
)

test("errors are preserved through serialization", () => {
  const { publish, serialize, source, o, deserialize } = setup(o => o.map(t))
  publish({})
  const result = deserialize(source, serialize(o))
  expect(result.errorObservable()).toEqual({ frames: [{ operator: 'map', position: 1 }], error })
})

// removing for now - not sure this is a safe assumption and we're handling this in the host anyway
// test("expressions do not receive messages if they are in an error state", () => {
//   const { publish, o, serialize, source, deserialize } = setup(
//     o => o.map(value => { if(value === 1) throw new Error('test') })
//   )
//   const spy = jest.fn()
//   o.subscribe(spy)
//   publish(0)
//   expect(spy.mock.calls.length).toBe(1)
//   publish(1)
//   publish(0)
//
//   const serialized = deserialize(source, serialize(o))
//   serialized.subscribe(spy)
//   publish(0)
//   expect(spy.mock.calls.length).toBe(1)
// })