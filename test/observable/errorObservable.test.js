const errorObservable = require('../../src/observable/errorObservable')

const error = new Error('test')

test("report attaches context as frame", () => {
  const o = errorObservable(undefined, { p1: 1 })
  o.addContext({ p2: 2 })
  const spy = jest.fn()
  o.subscribe(spy)
  o.report(error)
  expect(spy.mock.calls).toEqual([[{
    frames: [{ p1: 1, p2: 2 }],
    error
  }]])
})

test("addSource flows frames from child", () => {
  const o = errorObservable(undefined, { p1: 1 })
  const childSource = errorObservable(undefined, { c1: 1 })
  o.addSource({ errorObservable: childSource }, { source: 'child' })
  const spy = jest.fn()
  o.subscribe(spy)
  childSource.report(error)
  expect(spy.mock.calls).toEqual([[{
    frames: [
      { p1: 1, source: 'child' },
      { c1: 1 }
    ],
    error
  }]])
})

test("removeSource unsubscribes from child", () => {
  const o = errorObservable(undefined, { p1: 1 })
  const childSourceError = errorObservable(undefined, { c1: 1 })
  const childSource = { errorObservable: childSourceError }
  o.addSource(childSource, { source: 'child' })
  o.removeSource(childSource)
  const spy = jest.fn()
  o.subscribe(spy)
  childSourceError.report(error)
  expect(spy.mock.calls.length).toBe(0)
})
