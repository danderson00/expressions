const observable = require('../../src/observable/serializable')

test("serializable sets initial serializationInfo", () => {
  const o = observable(() => {}, { initialValue: 10 })
  expect(o.getSerializationInfo()).toEqual({
    definition: { returns: 'stream' },
    state: { currentValue: 10 }
  })
})

test("serializable passes default state to getState option", () => {
  const o = observable(
    () => {},
    {
      initialValue: 10,
      getState: state => {
        expect(state).toEqual({ currentValue: 10 })
        return 'executed'
      }
    }
  )
  expect(o.getSerializationInfo().state).toBe('executed')
})

test("serializable passes default definition to getDefinition option", () => {
  const o = observable(
    () => {},
    {
      initialValue: 10,
      getDefinition: definition => {
        expect(definition).toEqual({ returns: 'stream' })
        return 'executed'
      }
    }
  )
  expect(o.getSerializationInfo().definition).toBe('executed')
})

