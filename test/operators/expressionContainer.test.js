const expressionContainer = require('../../src/operators/core/expressionContainer')
const repository = require('../../src/repository')
const operators = require('../../src/operators')
const { subject } = require('../../src')
const { deserialize } = require('../../src/serialization/definition')

test("expressions are evaluated correctly", () => {
  const source = subject()
  const container = expressionContainer.create(
    repository(operators),
    source,
    o => o.where(x => x % 2 === 0).sum()
  )

  source.publish(2)
  source.publish(3)
  source.publish(4)

  expect(container()).toBe(6)
})

test("containers are serialized as unzipped observable", () => {
  const container = expressionContainer.create(
    repository(operators),
    subject(),
    o => o.where(x => x % 2 === 0).sum()
  )

  const info = container.getSerializationInfo()
  expect(info.definition.frames.length).toBe(2)
  expect(deserialize(info.definition.frames[0]).identifier).toBe('where')
  expect(deserialize(info.definition.frames[1]).identifier).toBe('sum')
  expect(info.state.frames.length).toBe(2)
})

test("containers can be serialized and deserialized", () => {
  const source1 = subject()
  const container = expressionContainer.create(
    repository(operators),
    source1,
    o => o.where(x => x % 2 === 0).sum()
  )
  source1.publish(2)

  const info = container.getSerializationInfo()
  const source2 = subject()
  const deserialized = expressionContainer.unpack(
    repository(operators),
    source2,
    info
  )

  expect(deserialized()).toBe(2)
  source2.publish(3)
  source2.publish(4)
  expect(deserialized()).toBe(6)
})

test("definition is extended with provided object", () => {
  const container = expressionContainer.create(
    repository(operators),
    subject(),
    o => o.where(x => x % 2 === 0).sum(),
    [],
    { p1: 1 }
  )
  expect(container.getSerializationInfo().definition.p1).toBe(1)
})