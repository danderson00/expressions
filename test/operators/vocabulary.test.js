const component = require('../../src/operators/vocabulary')
const operators = require('../../src/repository')(require('../../src/operators'))
const { subject } = require('../../src/observable')
const { stringify, parse } = require('../utils')

test("vocabulary returns observable from evaluating expression", () => {
  const source = subject()
  const vocabulary = component({
    name: 'sumOfEvens',
    expression: o => o.where(x => x % 2 ===0).sum()
  }).component(source, operators)
  const observable = vocabulary.create()

  expect(observable()).toBe(0)
  source.publish(2)
  expect(observable()).toBe(2)
  source.publish(3)  
  expect(observable()).toBe(2)
  source.publish(4)
  expect(observable()).toBe(6)
})

test("vocabulary can be parameterized", () => {
  const source = subject()
  const vocabulary = component({
    name: 'multiply',
    expression: (o, multiple) => o.map(x => x * multiple)
  }).component
  const observable = vocabulary(source, operators).create(3)

  source.publish(2)
  expect(observable()).toBe(6)
  source.publish(3)  
  expect(observable()).toBe(9)
  source.publish(4)
  expect(observable()).toBe(12)
})

test("vocabulary can be serialized", () => {
  const source = subject()
  const vocabulary = component({
    name: 'sumOfEvens',
    expression: o => o.where(x => x % 2 === 0).sum()
  }).component
  const observable = vocabulary(source, operators).create()
  source.publish(2)

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = vocabulary(source, operators).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toBe(2)
  source.publish(3)  
  expect(deserialized()).toBe(2)
  source.publish(4)
  expect(deserialized()).toBe(6)
})

test("parameterized vocabulary can be serialized", () => {
  const source = subject()
  const vocabulary = component({
    name: 'multiply',
    expression: (o, multiple) => o.map(x => x * multiple)
  }).component
  const observable = vocabulary(source, operators).create(3)
  source.publish(2)

  const serializationInfo = observable.getSerializationInfo()
  const deserializedSource = subject()
  const deserialized = vocabulary(deserializedSource, operators).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toBe(6)
  deserializedSource.publish(3)
  expect(deserialized()).toBe(9)
  deserializedSource.publish(4)
  expect(deserialized()).toBe(12)
})

test("vocabulary can be created from definition", () => {
  const source = subject()
  const vocabulary = component({
    name: 'sumOfEvens',
    expression: o => o.where(x => x % 2 ===0).sum()
  }).component
  const definition = vocabulary(source, operators).create().getSerializationInfo().definition
  const deserialized = vocabulary(source, operators).unpack({ definition })

  source.publish(2)
  expect(deserialized()).toBe(2)
  source.publish(3)  
  expect(deserialized()).toBe(2)
  source.publish(4)
  expect(deserialized()).toBe(6)
})

test("vocabulary can be serialized twice", () => {
  const source = subject()
  const vocabulary = component({
    name: 'sumOfEvens',
    expression: o => o.where(x => x % 2 ===0).sum()
  }).component
  const observable = vocabulary(source, operators).create()
  source.publish(2)

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = vocabulary(source, operators).unpack(parse(stringify(serializationInfo)))
  serializationInfo = observable.getSerializationInfo()
  deserialized = vocabulary(source, operators).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toBe(2)
  source.publish(3)  
  expect(deserialized()).toBe(2)
  source.publish(4)
  expect(deserialized()).toBe(6)
})

test("parameterized vocabulary can be serialized twice", () => {
  const source = subject()
  const vocabulary = component({
    name: 'multiply',
    expression: (o, multiple) => o.map(x => x * multiple)
  }).component
  const observable = vocabulary(source, operators).create(3)
  source.publish(2)

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = vocabulary(subject(), operators).unpack(parse(stringify(serializationInfo)))
  serializationInfo = deserialized.getSerializationInfo()
  const deserializedSource = subject()
  deserialized = vocabulary(deserializedSource, operators).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toBe(6)
  deserializedSource.publish(3)  
  expect(deserialized()).toBe(9)
  deserializedSource.publish(4)
  expect(deserialized()).toBe(12)
})

