const reduce = require('../../../src/operators/stream/reduce').component
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

test("reduce performs reduce operation over messages", () => {
  const source = subject()
  const handler = jest.fn()
  const observable = reduce(source).create((sum, next) => sum + next, 10)
  observable.subscribe(handler)
  expect(observable()).toBe(10)
  source.publish(1)
  expect(observable()).toBe(11)
  source.publish(2)  
  expect(observable()).toBe(13)
  source.publish(3)
  expect(observable()).toBe(16)
  expect(handler.mock.calls.length).toEqual(3)
  expect(handler.mock.calls.map(x => x[0])).toEqual([11, 13, 16])
})

test("reduce can be serialized", () => {
  const source = subject()
  const observable = reduce(source).create((sum, next) => sum + next, 10)
  source.publish(1)

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = reduce(source).unpack(parse(stringify(serializationInfo)))
  const handler = jest.fn()
  deserialized.subscribe(handler)

  expect(deserialized()).toBe(11)
  source.publish(2)
  expect(deserialized()).toBe(13)
  expect(handler.mock.calls.map(x => x[0])).toEqual([13])  
})

test("reduce can be created from definition", () => {
  const source = subject()
  const definition = reduce(source).create((sum, next) => sum + next, 10).getSerializationInfo().definition
  const deserialized = reduce(source).unpack({ definition })
  const handler = jest.fn()
  deserialized.subscribe(handler)

  expect(deserialized()).toBe(10)
  source.publish(1)
  expect(deserialized()).toBe(11)
  source.publish(2)
  expect(deserialized()).toBe(13)
})

test("reduce can be serialized twice", () => {  
  const source = subject()
  const observable = reduce(source).create((sum, next) => sum + next, 10)
  source.publish(1)

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = reduce(source).unpack(parse(stringify(serializationInfo)))

  serializationInfo = deserialized.getSerializationInfo()
  deserialized = reduce(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toBe(11)
  source.publish(2)
  expect(deserialized()).toBe(13)
})