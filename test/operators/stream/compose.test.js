const compose = require('../../../src/operators/stream/compose').component
const operators = require('../../../src/repository')(require('../../../src/operators'))
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

test("compose aggregates multiple expressions over single source", () => {
  const source = subject()
  const o = compose(source, operators).create(
    o => o.where(x => x % 2 === 0).sum(),
    o => o.map(x => x * 2),
    (sumOfEvens, double) => sumOfEvens + double
  )
  expect(o()).toBe(NaN)
  source.publish(2)
  expect(o()).toBe((2) + (2 * 2))
  source.publish(1)
  expect(o()).toBe((2) + (1 * 2))
  source.publish(10)
  expect(o()).toBe((2 + 10) + (10 * 2))
})

test("compose can be serialized", () => {
  const source = subject()
  const o = compose(source, operators).create(
    o => o.where(x => x % 2 === 0).sum(),
    o => o.map(x => x * 2),
    (sumOfEvens, double) => sumOfEvens + double
  )
  source.publish(2)

  const serializationInfo = o.getSerializationInfo()
  const deserialized = compose(source, operators).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toBe((2) + (2 * 2))
  source.publish(1)
  expect(deserialized()).toBe((2) + (1 * 2))
  source.publish(10)
  expect(deserialized()).toBe((2 + 10) + (10 * 2))
})

test("compose can be created from definition", () => {
  const source = subject()
  const definition = compose(source, operators).create(
    o => o.where(x => x % 2 === 0).sum(),
    o => o.map(x => x * 2),
    (sumOfEvens, double) => sumOfEvens + double
  ).getSerializationInfo().definition
  const deserialized = compose(source, operators).unpack({ definition })

  source.publish(2)
  expect(deserialized()).toBe((2) + (2 * 2))
  source.publish(1)
  expect(deserialized()).toBe((2) + (1 * 2))
  source.publish(10)
  expect(deserialized()).toBe((2 + 10) + (10 * 2))
})

test("compose can be serialized twice", () => {
  const source = subject()
  const o = compose(source, operators).create(
    o => o.where(x => x % 2 === 0).sum(),
    o => o.map(x => x * 2),
    (sumOfEvens, double) => sumOfEvens + double
  )
  source.publish(2)

  let serializationInfo = o.getSerializationInfo()
  let deserialized = compose(source, operators).unpack(parse(stringify(serializationInfo)))

  serializationInfo = deserialized.getSerializationInfo()
  deserialized = compose(source, operators).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toBe((2) + (2 * 2))
  source.publish(1)
  expect(deserialized()).toBe((2) + (1 * 2))
  source.publish(10)
  expect(deserialized()).toBe((2 + 10) + (10 * 2))
})

