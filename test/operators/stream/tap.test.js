const tap = require('../../../src/operators/stream/tap').component
const { subject } = require('../../../src/observable')

test("tap passes messages to sink", () => {
  const source = subject()
  const handler = jest.fn()
  const sink = jest.fn()
  const observable = tap(source).create(sink)
  observable.subscribe(handler)
  expect(observable.currentValue()).toBe(undefined)
  source.publish(1)
  expect(observable.currentValue()).toBe(1)
  expect(handler.mock.calls.map(x => x[0])).toEqual([1])
  expect(sink.mock.calls.map(x => x[0])).toEqual([1])
})

