const count = require('../../../src/operators/stream/count').component
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

test("count accumulates count of stream values", () => {
  const source = subject()
  const o = count(source).create()
  expect(o()).toBe(0)
  source.publish()
  expect(o()).toBe(1)
  source.publish()
  expect(o()).toBe(2)
  source.publish()
  expect(o()).toBe(3)
})

test("count can be serialized", () => {
  const source = subject()
  const o = count(source).create()
  source.publish()
  source.publish()

  const serializationInfo = o.getSerializationInfo()
  const deserialized = count(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toBe(2)
  source.publish()
  expect(deserialized()).toBe(3)
})

test("count can be created from definition", () => {
  const source = subject()
  const definition = count(source).create().getSerializationInfo().definition
  const deserialized = count(source).unpack({ definition })

  expect(deserialized()).toBe(0)
  source.publish()
  source.publish()
  expect(deserialized()).toBe(2)
})

test("count can be serialized twice", () => {
  const source = subject()
  const o = count(source).create()
  source.publish()

  let serializationInfo = o.getSerializationInfo()
  let deserialized = count(source).unpack(parse(stringify(serializationInfo)))
  serializationInfo = deserialized.getSerializationInfo()
  deserialized = count(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toBe(1)
  source.publish()
  expect(deserialized()).toBe(2)
})