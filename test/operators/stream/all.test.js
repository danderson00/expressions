const all = require('../../../src/operators/stream/all').component
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

test("all maintains array of all messages", () => {
  const source = subject()
  const observable = all(source).create()
  expect(observable.currentValue()).toEqual([])
  source.publish(1)
  expect(observable.currentValue()).toEqual([1])
  source.publish(2)
  expect(observable.currentValue()).toEqual([2, 1])
  source.publish(3)
  expect(observable.currentValue()).toEqual([3, 2, 1])
})

test("all can be serialized", () => {
  const source = subject()
  const observable = all(source).create(2)
  source.publish(1)

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = all(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized.currentValue()).toEqual([1])
  source.publish(2)
  expect(deserialized.currentValue()).toEqual([2, 1])
  source.publish(3)
  expect(deserialized.currentValue()).toEqual([3, 2, 1])
})

test("all can be created from definition", () => {
  const source = subject()
  const definition = all(source).create(2).getSerializationInfo().definition
  const deserialized = all(source).unpack({ definition })

  expect(deserialized.currentValue()).toEqual([])
  source.publish(1)
  expect(deserialized.currentValue()).toEqual([1])
  source.publish(2)
  expect(deserialized.currentValue()).toEqual([2, 1])
  source.publish(3)
  expect(deserialized.currentValue()).toEqual([3, 2, 1])
})

test("all can be serialized twice", () => {
  const source = subject()
  const observable = all(source).create(2)
  source.publish(1)

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = all(source).unpack(parse(stringify(serializationInfo)))
  serializationInfo = deserialized.getSerializationInfo()
  deserialized = all(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized.currentValue()).toEqual([1])
  source.publish(2)
  expect(deserialized.currentValue()).toEqual([2, 1])
  source.publish(3)
  expect(deserialized.currentValue()).toEqual([3, 2, 1])
})