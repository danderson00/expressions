const accumulate = require('../../../src/operators/stream/accumulate').component
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

test("accumulate merges object structure of messages", () => {
  const source = subject()
  const o = accumulate(source).create()
  expect(o()).toEqual({})
  source.publish({ p1: 1 })
  expect(o()).toEqual({ p1: 1 })
  source.publish({ p1: 2, p2: 'test' })
  expect(o()).toEqual({ p1: 2, p2: 'test' })
})

test("accumulate evaluates expression", () => {
  const source = subject()
  const o = accumulate(source).create('value')
  expect(o()).toEqual({})
  source.publish({ value: { p1: 1 } })
  expect(o()).toEqual({ p1: 1 })
  source.publish({ value: { p1: 2, p2: 'test' } })
  expect(o()).toEqual({ p1: 2, p2: 'test' })
})

test("accumulate can be serialized", () => {
  const source = subject()
  const o = accumulate(source).create()
  source.publish({ p1: 1 })

  const serializationInfo = o.getSerializationInfo()
  const deserialized = accumulate(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toEqual({ p1: 1 })
  source.publish({ p1: 2, p2: 'test' })
  expect(deserialized()).toEqual({ p1: 2, p2: 'test' })
})

test("accumulate with expression can be serialized", () => {
  const source = subject()
  const o = accumulate(source).create('value')
  source.publish({ value: { p1: 1 } })

  const serializationInfo = o.getSerializationInfo()
  const deserialized = accumulate(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toEqual({ p1: 1 })
  source.publish({ value: { p1: 2, p2: 'test' } })
  expect(deserialized()).toEqual({ p1: 2, p2: 'test' })
})

test("accumulate can be serialized twice", () => {
  const source = subject()
  const o = accumulate(source).create()
  source.publish({ p1: 1 })

  let serializationInfo = o.getSerializationInfo()
  let deserialized = accumulate(source).unpack(parse(stringify(serializationInfo)))
  serializationInfo = deserialized.getSerializationInfo()
  deserialized = accumulate(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toEqual({ p1: 1 })
  source.publish({ p1: 2, p2: 'test' })
  expect(deserialized()).toEqual({ p1: 2, p2: 'test' })
})