const average = require('../../../src/operators/stream/average').component
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

test("average accumulates average of stream values", () => {
  const source = subject()
  const observable = average(source).create()
  expect(observable()).toBeUndefined()
  source.publish(1)
  expect(observable()).toBe(1)
  source.publish(3)
  expect(observable()).toBe(2)
  source.publish(5)
  expect(observable()).toBe(3)
})

test("average accumulates average of result of expression", () => {
  const source = subject()
  const observable = average(source).create(x => x.value)
  expect(observable()).toBeUndefined()
  source.publish({ value: 1 })
  expect(observable()).toBe(1)
  source.publish({ value: 3 })
  expect(observable()).toBe(2)
  source.publish({ value: 5 })
  expect(observable()).toBe(3)
})

test("average accumulates average of property name", () => {
  const source = subject()
  const observable = average(source).create('value')
  expect(observable()).toBeUndefined()
  source.publish({ value: 1 })
  expect(observable()).toBe(1)
  source.publish({ value: 3 })
  expect(observable()).toBe(2)
  source.publish({ value: 5 })
  expect(observable()).toBe(3)
})

test("average of stream values can be serialized", () => {
  const source = subject()
  const observable = average(source).create()
  source.publish(1)
  source.publish(3)

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = average(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toBe(2)
  source.publish(5)
  expect(deserialized()).toBe(3)
})

test("average of expression results can be serialized", () => {
  const source = subject()
  const observable = average(source).create(x => x.value)
  source.publish({ value: 1 })
  source.publish({ value: 3 })

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = average(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toBe(2)
  source.publish({ value: 5 })
  expect(deserialized()).toBe(3)
})

test("average of property name can be serialized", () => {
  const source = subject()
  const observable = average(source).create('value')
  source.publish({ value: 1 })
  source.publish({ value: 3 })

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = average(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toBe(2)
  source.publish({ value: 5 })
  expect(deserialized()).toBe(3)
})

test("average can be created from definition", () => {
  const source = subject()
  const definition = average(source).create(x => x.value).getSerializationInfo().definition
  const deserialized = average(source).unpack({ definition })

  expect(deserialized()).toBeUndefined()
  source.publish({ value: 1 })
  source.publish({ value: 3 })
  expect(deserialized()).toBe(2)
})

test("average of expression results can be serialized twice", () => {
  const source = subject()
  const observable = average(source).create(x => x.value)
  source.publish({ value: 1 })
  source.publish({ value: 3 })

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = average(source).unpack(parse(stringify(serializationInfo)))
  serializationInfo = deserialized.getSerializationInfo()
  deserialized = average(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toBe(2)
  source.publish({ value: 5 })
  expect(deserialized()).toBe(3)
})

test("average of property name can be serialized twice", () => {
  const source = subject()
  const observable = average(source).create('value')
  source.publish({ value: 1 })

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = average(source).unpack(parse(stringify(serializationInfo)))
  serializationInfo = deserialized.getSerializationInfo()
  deserialized = average(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toBe(1)
  source.publish({ value: 3 })
  expect(deserialized()).toBe(2)
})