const first = require('../../../src/operators/stream/first').component
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

test("first returns first message", () => {
  const source = subject()
  const observable = first(source).create()
  expect(observable.currentValue()).toBeUndefined()
  source.publish(1)
  expect(observable.currentValue()).toBe(1)
  source.publish(2)
  expect(observable.currentValue()).toBe(1)
  source.publish(3)
  expect(observable.currentValue()).toBe(1)
})

test("first can be serialized", () => {
  const source = subject()
  const observable = first(source).create()
  source.publish(1)

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = first(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized.currentValue()).toBe(1)
  source.publish(2)
  expect(deserialized.currentValue()).toBe(1)
  source.publish(3)
  expect(deserialized.currentValue()).toBe(1)
})

test("first can be created from definition", () => {
  const source = subject()
  const definition = first(source).create().getSerializationInfo().definition
  const deserialized = first(source).unpack({ definition })

  expect(deserialized.currentValue()).toBeUndefined()
  source.publish(1)
  expect(deserialized.currentValue()).toBe(1)
  source.publish(2)
  expect(deserialized.currentValue()).toBe(1)
  source.publish(3)
  expect(deserialized.currentValue()).toBe(1)
})

test("first can be serialized twice", () => {
  const source = subject()
  const observable = first(source).create()
  source.publish(1)

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = first(source).unpack(parse(stringify(serializationInfo)))
  serializationInfo = deserialized.getSerializationInfo()
  deserialized = first(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized.currentValue()).toBe(1)
  source.publish(2)
  expect(deserialized.currentValue()).toBe(1)
  source.publish(3)
  expect(deserialized.currentValue()).toBe(1)
})