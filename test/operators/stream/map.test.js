const map = require('../../../src/operators/stream/map').component
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

test("map transforms messages", () => {
  const source = subject()
  const handler = jest.fn()
  const observable = map(source).create(x => x * 2)
  observable.subscribe(handler)
  expect(observable.currentValue()).toBe(undefined)
  source.publish(1)
  expect(observable.currentValue()).toBe(2)
  source.publish(2)  
  expect(observable.currentValue()).toBe(4)
  source.publish(3)
  expect(observable.currentValue()).toBe(6)
  expect(handler.mock.calls.length).toEqual(3)
  expect(handler.mock.calls.map(x => x[0])).toEqual([2, 4, 6])
})

test("map can be serialized", () => {
  const source = subject()
  const observable = map(source).create(x => x * 2)
  source.publish(1)

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = map(source).unpack(parse(stringify(serializationInfo)))
  const handler = jest.fn()
  deserialized.subscribe(handler)

  expect(deserialized.currentValue()).toBe(2)
  source.publish(2)
  expect(deserialized.currentValue()).toBe(4)
  expect(handler.mock.calls.map(x => x[0])).toEqual([4])  
})

test("map can be created from definition", () => {
  const source = subject()
  const definition = map(source).create(x => x * 2).getSerializationInfo().definition
  const deserialized = map(source).unpack({ definition })
  const handler = jest.fn()
  deserialized.subscribe(handler)

  expect(deserialized.currentValue()).toBeUndefined()
  source.publish(1)
  expect(deserialized.currentValue()).toBe(2)
  source.publish(2)
  expect(deserialized.currentValue()).toBe(4)
})

test("map can be serialized twice", () => {  
  const source = subject()
  const observable = map(source).create(x => x * 2)
  source.publish(1)

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = map(source).unpack(parse(stringify(serializationInfo)))

  serializationInfo = deserialized.getSerializationInfo()
  deserialized = map(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized.currentValue()).toBe(2)
  source.publish(2)
  expect(deserialized.currentValue()).toBe(4)
})