const takeLast = require('../../../src/operators/stream/takeLast').component
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

test("takeLast maintains array of last n messages", () => {
  const source = subject()
  const observable = takeLast(source).create(2)
  expect(observable.currentValue()).toEqual([])
  source.publish(1)
  expect(observable.currentValue()).toEqual([1])
  source.publish(2)
  expect(observable.currentValue()).toEqual([2, 1])
  source.publish(3)
  expect(observable.currentValue()).toEqual([3, 2])
})

test("takeLast can be serialized", () => {
  const source = subject()
  const observable = takeLast(source).create(2)
  source.publish(1)

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = takeLast(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized.currentValue()).toEqual([1])
  source.publish(2)
  expect(deserialized.currentValue()).toEqual([2, 1])
  source.publish(3)
  expect(deserialized.currentValue()).toEqual([3, 2])
})

test("takeLast can be created from definition", () => {
  const source = subject()
  const definition = takeLast(source).create(2).getSerializationInfo().definition
  const deserialized = takeLast(source).unpack({ definition })

  expect(deserialized.currentValue()).toEqual([])
  source.publish(1)
  expect(deserialized.currentValue()).toEqual([1])
  source.publish(2)
  expect(deserialized.currentValue()).toEqual([2, 1])
  source.publish(3)
  expect(deserialized.currentValue()).toEqual([3, 2])
})

test("takeLast can be serialized twice", () => {
  const source = subject()
  const observable = takeLast(source).create(2)
  source.publish(1)

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = takeLast(source).unpack(parse(stringify(serializationInfo)))
  serializationInfo = deserialized.getSerializationInfo()
  deserialized = takeLast(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized.currentValue()).toEqual([1])
  source.publish(2)
  expect(deserialized.currentValue()).toEqual([2, 1])
  source.publish(3)
  expect(deserialized.currentValue()).toEqual([3, 2])
})