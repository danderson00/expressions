const select = require('../../../src/operators/stream/select').component
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

test("select with single argument selects single property value", () => {
  const source = subject()
  const handler = jest.fn()
  const observable = select(source).create('p1')
  observable.subscribe(handler)
  expect(observable()).toBe(undefined)
  source.publish({ p1: 1 })
  expect(observable()).toBe(1)
  source.publish({ p1: 'test' })
  expect(observable()).toBe('test')
  source.publish({ p1: false })
  expect(observable()).toBe(false)
  expect(handler.mock.calls.length).toEqual(3)
  expect(handler.mock.calls.map(x => x[0])).toEqual([1, 'test', false])
})

test("select with multiple arguments selects property values into a hash", () => {
  const source = subject()
  const handler = jest.fn()
  const observable = select(source).create('p1', 'p2', 'p3')
  observable.subscribe(handler)
  expect(observable()).toBeUndefined()
  source.publish({ p1: 1, p2: 'test', p3: false })
  expect(observable()).toEqual({ p1: 1, p2: 'test', p3: false })
  source.publish({ p1: 2, p2: 'test2' })
  expect(observable()).toEqual({ p1: 2, p2: 'test2', p3: undefined })
  source.publish({ p1: 3, p2: 'test3', p3: true })
  expect(observable()).toEqual({ p1: 3, p2: 'test3', p3: true })
})

test("select with single argument can be serialized", () => {
  const source = subject()
  const observable = select(source).create('p1')
  source.publish({ p1: 1 })

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = select(source).unpack(parse(stringify(serializationInfo)))
  const handler = jest.fn()
  deserialized.subscribe(handler)

  expect(deserialized()).toBe(1)
  source.publish({ p1: 'test' })
  expect(deserialized()).toBe('test')
  source.publish({ p1: false })
  expect(deserialized()).toBe(false)
})

test("select with multiple arguments can be serialized", () => {
  const source = subject()
  const observable = select(source).create('p1', 'p2', 'p3')
  source.publish({ p1: 1, p2: 'test', p3: false })

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = select(source).unpack(parse(stringify(serializationInfo)))
  const handler = jest.fn()
  deserialized.subscribe(handler)

  expect(deserialized()).toEqual({ p1: 1, p2: 'test', p3: false })
  source.publish({ p1: 2, p2: 'test2' })
  expect(deserialized()).toEqual({ p1: 2, p2: 'test2', p3: undefined })
  source.publish({ p1: 3, p2: 'test3', p3: true })
  expect(deserialized()).toEqual({ p1: 3, p2: 'test3', p3: true })
})

test("select with single argument can be created from definition", () => {
  const source = subject()
  const definition = select(source).create('p1').getSerializationInfo().definition
  const deserialized = select(source).unpack({ definition })
  const handler = jest.fn()
  deserialized.subscribe(handler)

  expect(deserialized()).toBeUndefined()
  source.publish({ p1: 1 })
  expect(deserialized()).toBe(1)
  source.publish({ p1: 'test' })
  expect(deserialized()).toBe('test')
})

test("select with multiple arguments can be created from definition", () => {
  const source = subject()
  const definition = select(source).create('p1', 'p2', 'p3').getSerializationInfo().definition
  const deserialized = select(source).unpack({ definition })
  const handler = jest.fn()
  deserialized.subscribe(handler)

  expect(deserialized()).toBeUndefined()
  source.publish({ p1: 1, p2: 'test', p3: false })
  expect(deserialized()).toEqual({ p1: 1, p2: 'test', p3: false })
  source.publish({ p1: 2, p2: 'test2' })
  expect(deserialized()).toEqual({ p1: 2, p2: 'test2', p3: undefined })
})

test("select with single argument can be serialized twice", () => {  
  const source = subject()
  const observable = select(source).create('p1')
  source.publish({ p1: 1 })

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = select(source).unpack(parse(stringify(serializationInfo)))

  serializationInfo = deserialized.getSerializationInfo()
  deserialized = select(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toBe(1)
  source.publish({ p1: 2 })
  expect(deserialized()).toBe(2)
})

test("select with multiple arguments can be serialized twice", () => {  
  const source = subject()
  const observable = select(source).create('p1', 'p2', 'p3')
  source.publish({ p1: 1, p2: 'test', p3: false })

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = select(source).unpack(parse(stringify(serializationInfo)))

  serializationInfo = deserialized.getSerializationInfo()
  deserialized = select(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toEqual({ p1: 1, p2: 'test', p3: false })
  source.publish({ p1: 2, p2: 'test2' })
  expect(deserialized()).toEqual({ p1: 2, p2: 'test2', p3: undefined })
})