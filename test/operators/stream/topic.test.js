const topic = require('../../../src/operators/stream/topic').component
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

test("topic filters messages based on topic property", () => {
  const source = subject()
  const observable = topic(source).create('t1', 't2')
  source.publish({ topic: 't1' })
  expect(observable()).toEqual({ topic: 't1' })
  source.publish({ topic: 't2' })
  expect(observable()).toEqual({ topic: 't2' })
  source.publish({ topic: 't3' })
  expect(observable()).toEqual({ topic: 't2' })
  source.publish(1)
  expect(observable()).toEqual({ topic: 't2' })
})

test("topic can be serialized", () => {
  const source = subject()
  const observable = topic(source).create('t1', 't2')
  source.publish({ topic: 't1' })

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = topic(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toEqual({ topic: 't1' })
  source.publish({ topic: 't2' })
  expect(deserialized()).toEqual({ topic: 't2' })
})

test("topic can be created from definition", () => {
  const source = subject()
  const definition = topic(source).create('t1', 't2').getSerializationInfo().definition
  const deserialized = topic(source).unpack({ definition })

  source.publish({ topic: 't1' })
  expect(deserialized()).toEqual({ topic: 't1' })
  source.publish({ topic: 't2' })
  expect(deserialized()).toEqual({ topic: 't2' })
})

test("topic can be serialized twice", () => {  
  const source = subject()
  const observable = topic(source).create('t1', 't2')
  source.publish({ topic: 't1' })

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = topic(source).unpack(parse(stringify(serializationInfo)))

  serializationInfo = deserialized.getSerializationInfo()
  deserialized = topic(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toEqual({ topic: 't1' })
  source.publish({ topic: 't2' })
  expect(deserialized()).toEqual({ topic: 't2' })
})