const groupBy = require('../../../src/operators/stream/groupBy')(false).component
const operators = require('../../../src/repository')(require('../../../src/operators'))
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')
const { unwrap } = require('../../../src/utilities')

test("groupBy groups messages by expression", () => {
  const source = subject()
  const groups = groupBy(source, operators).create(x => x % 2)
  expect(groups()).toEqual([])
  source.publish(1)
  expect(unwrap(groups)).toEqual([1])
  expect(groups().map(x => x.key)).toEqual([1])
  source.publish(2)
  expect(unwrap(groups)).toEqual([1, 2])
  expect(groups().map(x => x.key)).toEqual([1, 0])
  source.publish(3)
  expect(unwrap(groups)).toEqual([3, 2])
  expect(groups().map(x => x.key)).toEqual([1, 0])
})

test("groupBy groups messages by property name", () => {
  const source = subject()
  const groups = groupBy(source, operators).create('category')
  expect(groups()).toEqual([])
  source.publish({ category: 1 })
  expect(unwrap(groups)).toEqual([{ category: 1 }])
  expect(groups().map(x => x.key)).toEqual([1])
  source.publish({ category: 2 })
  expect(unwrap(groups)).toEqual([{ category: 1 }, { category: 2 }])
  expect(groups().map(x => x.key)).toEqual([1, 2])
})

test("groupBy applies groupExpression to groups", () => {
  const source = subject()
  const groups = groupBy(source, operators).create(x => x % 2, o => o.map(x => x * 2))
  expect(groups()).toEqual([])
  source.publish(1)
  expect(unwrap(groups)).toEqual([2])
  expect(groups().map(x => x.key)).toEqual([1])
  source.publish(2)
  expect(unwrap(groups)).toEqual([2, 4])
  expect(groups().map(x => x.key)).toEqual([1, 0])
  source.publish(3)
  expect(unwrap(groups)).toEqual([6, 4])
  expect(groups().map(x => x.key)).toEqual([1, 0])
})

test("groupBy passes key as second parameter to groupExpression", () => {
  const source = subject()
  const groups = groupBy(source, operators).create(x => x % 2, (o, key) => subject({ initialValue: key }))
  source.publish(1)
  expect(unwrap(groups)).toEqual([1])
  source.publish(2)
  expect(unwrap(groups)).toEqual([1, 0])
})

test("groupBy exposes key in groupExpression", () => {
  const source = subject()
  const groups = groupBy(source, operators).create(x => x % 2, o => subject({ initialValue: o.key }))
  source.publish(1)
  expect(unwrap(groups)).toEqual([1])
  source.publish(2)
  expect(unwrap(groups)).toEqual([1, 0])
})

test("groupBy removes groups when removeExpression is truthy", () => {
  const source = subject()
  const groups = groupBy(source, operators).create('category', undefined, o => o.where(x => x.remove))
  source.publish({ category: 1 })
  source.publish({ category: 2 })
  source.publish({ category: 1, remove: true })
  expect(unwrap(groups)).toEqual([{ category: 2 }])
  expect(groups().map(x => x.key)).toEqual([2])
})

test("groupExpression.disconnect removes group", () => {
  const source = subject()
  const groups = groupBy(source, operators).create('category', undefined, o => o.where(x => x.remove))
  source.publish({ category: 1 })
  source.publish({ category: 2 })
  groups()[0].disconnect()
  expect(unwrap(groups)).toEqual([{ category: 2 }])
  expect(groups().map(x => x.key)).toEqual([2])
})

test("groupBy can be serialized", () => {
  const source = subject()
  const groups = groupBy(source, operators).create(x => x % 2)
  source.publish(1)

  const serializationInfo = groups.getSerializationInfo()
  const deserialized = groupBy(source, operators).unpack(parse(stringify(serializationInfo)))

  expect(unwrap(deserialized)).toEqual([1])
  expect(deserialized().map(x => x.key)).toEqual([1])
  source.publish(2)
  expect(unwrap(deserialized)).toEqual([1, 2])
  expect(deserialized().map(x => x.key)).toEqual([1, 0])
  source.publish(3)
  expect(unwrap(deserialized)).toEqual([3, 2])
  expect(deserialized().map(x => x.key)).toEqual([1, 0])
})

test("groupBy with groupExpression can be serialized", () => {
  const source = subject()
  const groups = groupBy(source, operators).create(x => x % 2, o => o.map(x => x * 2))
  source.publish(1)

  const serializationInfo = groups.getSerializationInfo()
  const deserialized = groupBy(source, operators).unpack(parse(stringify(serializationInfo)))

  expect(unwrap(deserialized)).toEqual([2])
  expect(deserialized().map(x => x.key)).toEqual([1])
  source.publish(2)
  expect(unwrap(deserialized)).toEqual([2, 4])
  expect(deserialized().map(x => x.key)).toEqual([1, 0])
  source.publish(3)
  expect(unwrap(deserialized)).toEqual([6, 4])
  expect(deserialized().map(x => x.key)).toEqual([1, 0])
})

test("groupBy with groupExpression using key parameter can be serialized", () => {
  const source = subject()
  const groups = groupBy(source, operators).create(x => x % 2, (o, key) => o.map(() => key))
  source.publish(1)

  const serializationInfo = groups.getSerializationInfo()
  const deserialized = groupBy(source, operators).unpack(parse(stringify(serializationInfo)))

  expect(unwrap(deserialized)).toEqual([1])
  source.publish(2)
  expect(unwrap(deserialized)).toEqual([1, 0])
})

test("groupBy with property name can be serialized", () => {
  const source = subject()
  const groups = groupBy(source, operators).create('category')
  source.publish({ category: 1 })

  const serializationInfo = groups.getSerializationInfo()
  const deserialized = groupBy(source, operators).unpack(parse(stringify(serializationInfo)))

  expect(unwrap(deserialized)).toEqual([{ category: 1 }])
  expect(deserialized().map(x => x.key)).toEqual([1])
  source.publish({ category: 2 })
  expect(unwrap(deserialized)).toEqual([{ category: 1 }, { category: 2 }])
  expect(deserialized().map(x => x.key)).toEqual([1, 2])
})

test("groupBy with empty groupExpression can be serialized", () => {
  const source = subject()
  const groups = groupBy(source, operators).create(x => x % 2, o => o)
  source.publish(1)

  const serializationInfo = groups.getSerializationInfo()
  const deserialized = groupBy(source, operators).unpack(parse(stringify(serializationInfo)))

  expect(unwrap(deserialized)).toEqual([1])
  expect(deserialized().map(x => x.key)).toEqual([1])
  source.publish(2)
  expect(unwrap(deserialized)).toEqual([1, 2])
  expect(deserialized().map(x => x.key)).toEqual([1, 0])
})

test("groupBy serializes disconnected groups correctly", () => {
  const source = subject()
  const groups = groupBy(source, operators).create('category', undefined, o => o.where(x => x.remove))
  source.publish({ category: 1 })
  source.publish({ category: 2 })
  groups()[0].disconnect()

  const serializationInfo = groups.getSerializationInfo()
  const deserialized = groupBy(source, operators).unpack(parse(stringify(serializationInfo)))

  expect(unwrap(deserialized)).toEqual([{ category: 2 }])
  expect(deserialized().map(x => x.key)).toEqual([2])
})

test("groupBy can be created from definition", () => {
  const source = subject()
  const definition = groupBy(source, operators).create(x => x % 2).getSerializationInfo().definition
  const deserialized = groupBy(source, operators).unpack({ definition })

  source.publish(1)
  expect(unwrap(deserialized)).toEqual([1])
  expect(deserialized().map(x => x.key)).toEqual([1])
  source.publish(2)
  expect(unwrap(deserialized)).toEqual([1, 2])
  expect(deserialized().map(x => x.key)).toEqual([1, 0])
})

test("groupBy with groupExpression can be created from definition", () => {
  const source = subject()
  const definition = groupBy(source, operators).create(x => x % 2, o => o.map(x => x * 2)).getSerializationInfo().definition
  const deserialized = groupBy(source, operators).unpack({ definition })

  source.publish(1)
  expect(unwrap(deserialized)).toEqual([2])
  expect(deserialized().map(x => x.key)).toEqual([1])
  source.publish(2)
  expect(unwrap(deserialized)).toEqual([2, 4])
  expect(deserialized().map(x => x.key)).toEqual([1, 0])
})

test("groupBy with removeExpression can be serialized", () => {
  const source = subject()
  const groups = groupBy(source, operators).create('category', undefined, o => o.where(x => x.remove))

  source.publish({ category: 1 })
  source.publish({ category: 2 })

  const serializationInfo = groups.getSerializationInfo()
  const deserialized = groupBy(source, operators).unpack(parse(stringify(serializationInfo)))

  source.publish({ category: 1, remove: true })
  expect(unwrap(deserialized)).toEqual([{ category: 2 }])
  expect(deserialized().map(x => x.key)).toEqual([2])
})

test("groupBy can be serialized twice", () => {
  const source = subject()
  const observable = groupBy(source, operators).create(x => x % 2)
  source.publish(1)

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = groupBy(source, operators).unpack(parse(stringify(serializationInfo)))

  serializationInfo = deserialized.getSerializationInfo()
  deserialized = groupBy(source, operators).unpack(parse(stringify(serializationInfo)))

  expect(unwrap(deserialized)).toEqual([1])
  expect(deserialized().map(x => x.key)).toEqual([1])
  source.publish(2)
  expect(unwrap(deserialized)).toEqual([1, 2])
  expect(deserialized().map(x => x.key)).toEqual([1, 0])
})

test("groupBy with groupExpression can be serialized twice", () => {
  const source = subject()
  const observable = groupBy(source, operators).create(x => x % 2, o => o.map(x => x * 2))
  source.publish(1)

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = groupBy(source, operators).unpack(parse(stringify(serializationInfo)))

  serializationInfo = deserialized.getSerializationInfo()
  deserialized = groupBy(source, operators).unpack(parse(stringify(serializationInfo)))

  expect(unwrap(deserialized)).toEqual([2])
  expect(deserialized().map(x => x.key)).toEqual([1])
  source.publish(2)
  expect(unwrap(deserialized)).toEqual([2, 4])
  expect(deserialized().map(x => x.key)).toEqual([1, 0])
})

test("groupBy with groupExpression and removeExpression can be serialized twice", () => {
  const source = subject()
  const observable = groupBy(source, operators).create('category', o => o.select('category'), o => o.where(x => x.remove))

  source.publish({ category: 1 })
  source.publish({ category: 2 })

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = groupBy(source, operators).unpack(parse(stringify(serializationInfo)))

  serializationInfo = deserialized.getSerializationInfo()
  deserialized = groupBy(source, operators).unpack(parse(stringify(serializationInfo)))

  source.publish({ category: 1, remove: true })
  expect(unwrap(deserialized)).toEqual([2])
  expect(deserialized().map(x => x.key)).toEqual([2])
})

test("groupExpression can be specified as operator name", () => {
  const source = subject()
  const groups = groupBy(source, operators).create('category', 'count')

  source.publish({ category: 1 })
  source.publish({ category: 1 })
  source.publish({ category: 2 })
  expect(unwrap(groups)).toEqual([2, 1])

  let serializationInfo = groups.getSerializationInfo()
  let deserialized = groupBy(source, operators).unpack(parse(stringify(serializationInfo)))

  serializationInfo = deserialized.getSerializationInfo()
  deserialized = groupBy(source, operators).unpack(parse(stringify(serializationInfo)))

  expect(unwrap(deserialized)).toEqual([2, 1])
  source.publish({ category: 1 })
  source.publish({ category: 2 })
  source.publish({ category: 3 })
  expect(unwrap(deserialized)).toEqual([3, 2, 1])
})

// fails at this stage... see `groupBy.js`
// test("groupExpression can be specified as object composition", () => {
//   const source = subject()
//   const groups = groupBy(source, operators).create('category', {
//     category: o => o.select('category'),
//     count: o => o.count()
//   })
//
//   source.publish({ category: 1 })
//   source.publish({ category: 1 })
//   source.publish({ category: 2 })
//   expect(unwrap(groups)).toEqual([{ category: 1, count: 2 }, { category: 2, count: 1 }])
//
//   let serializationInfo = groups.getSerializationInfo()
//   let deserialized = groupBy(source, operators).unpack(parse(stringify(serializationInfo)))
//
//   serializationInfo = deserialized.getSerializationInfo()
//   deserialized = groupBy(source, operators).unpack(parse(stringify(serializationInfo)))
//
//   expect(unwrap(deserialized)).toEqual([{ category: 1, count: 2 }, { category: 2, count: 1 }])
//   source.publish({ category: 1 })
//   source.publish({ category: 2 })
//   source.publish({ category: 3 })
//   expect(unwrap(deserialized)).toEqual([{ category: 1, count: 3 }, { category: 2, count: 2 }, { category: 3, count: 1 }])
// })

// test("removeExpression operates correctly after deserialize", () => {})