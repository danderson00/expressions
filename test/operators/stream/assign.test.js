const assign = require('../../../src/operators/stream/assign').component
const operators = require('../../../src/repository')(require('../../../src/operators'))
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')
const { unwrap } = require('../../../src/utilities')

test("assign aggregates multiple expressions over single source", () => {
  const source = subject()
  const o = assign(source, operators).create({
    sumOfEvens: o => o.where(x => x % 2 === 0).sum(),
    '...double': o => o.map(x => ({ double: x * 2 }))
  })
  expect(o()).toEqual({ sumOfEvens: 0 })
  source.publish(2)
  expect(o()).toEqual({ sumOfEvens: 2, double: 4 })
  source.publish(1)
  expect(o()).toEqual({ sumOfEvens: 2, double: 2 })
  source.publish(10)
  expect(o()).toEqual({ sumOfEvens: 12, double: 20 })
})

test("assign can be serialized", () => {
  const source = subject()
  const o = assign(source, operators).create({
    sumOfEvens: o => o.where(x => x % 2 === 0).sum(),
    double: o => o.map(x => x * 2)
  })
  source.publish(2)
  expect(unwrap(o)).toEqual({ sumOfEvens: 2, double: 4 })

  const serializationInfo = o.getSerializationInfo()
  const d = assign(source, operators).unpack(parse(stringify(serializationInfo)))

  expect(unwrap(d)).toEqual({ sumOfEvens: 2, double: 4 })
  source.publish(1)
  expect(unwrap(d)).toEqual({ sumOfEvens: 2, double: 2 })
  source.publish(10)
  expect(unwrap(d)).toEqual({ sumOfEvens: 12, double: 20 })
})

test("assign can be created from definition", () => {
  const source = subject()
  const definition = assign(source, operators).create({
    sumOfEvens: o => o.where(x => x % 2 === 0).sum(),
    double: o => o.map(x => x * 2)
  }).getSerializationInfo().definition
  const d = assign(source, operators).unpack({ definition })

  source.publish(2)
  expect(unwrap(d)).toEqual({ sumOfEvens: 2, double: 4 })
  source.publish(1)
  expect(unwrap(d)).toEqual({ sumOfEvens: 2, double: 2 })
  source.publish(10)
  expect(unwrap(d)).toEqual({ sumOfEvens: 12, double: 20 })
})

test("assign can be serialized twice", () => {
  const source = subject()
  const o = assign(source, operators).create({
    sumOfEvens: o => o.where(x => x % 2 === 0).sum(),
    double: o => o.map(x => x * 2)
  })
  source.publish(2)
  expect(unwrap(o)).toEqual({ sumOfEvens: 2, double: 4 })

  let serializationInfo = o.getSerializationInfo()
  let d = assign(source, operators).unpack(parse(stringify(serializationInfo)))

  serializationInfo = d.getSerializationInfo()
  d = assign(source, operators).unpack(parse(stringify(serializationInfo)))

  expect(unwrap(d)).toEqual({ sumOfEvens: 2, double: 4 })
  source.publish(1)
  expect(unwrap(d)).toEqual({ sumOfEvens: 2, double: 2 })
  source.publish(10)
  expect(unwrap(d)).toEqual({ sumOfEvens: 12, double: 20 })
})

test("assign throws on creation if expression is invalid", () => {
  const source = subject()
  expect(() => assign(source, operators).create({
    invalid: o => o.nonExistent()
  })).toThrow()
})
