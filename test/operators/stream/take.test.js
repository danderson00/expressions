const take = require('../../../src/operators/stream/take').component
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

test("take maintains array of first n messages", () => {
  const source = subject()
  const observable = take(source).create(2)
  expect(observable.currentValue()).toEqual([])
  source.publish(1)
  expect(observable.currentValue()).toEqual([1])
  source.publish(2)
  expect(observable.currentValue()).toEqual([1, 2])
  source.publish(3)
  expect(observable.currentValue()).toEqual([1, 2])
})

test("take can be serialized", () => {
  const source = subject()
  const observable = take(source).create(2)
  source.publish(1)

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = take(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized.currentValue()).toEqual([1])
  source.publish(2)
  expect(deserialized.currentValue()).toEqual([1, 2])
  source.publish(3)
  expect(deserialized.currentValue()).toEqual([1, 2])
})

test("take can be created from definition", () => {
  const source = subject()
  const definition = take(source).create(2).getSerializationInfo().definition
  const deserialized = take(source).unpack({ definition })

  expect(deserialized.currentValue()).toEqual([])
  source.publish(1)
  expect(deserialized.currentValue()).toEqual([1])
  source.publish(2)
  expect(deserialized.currentValue()).toEqual([1, 2])
  source.publish(3)
  expect(deserialized.currentValue()).toEqual([1, 2])
})

test("take can be serialized twice", () => {
  const source = subject()
  const observable = take(source).create(2)
  source.publish(1)

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = take(source).unpack(parse(stringify(serializationInfo)))
  serializationInfo = deserialized.getSerializationInfo()
  deserialized = take(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized.currentValue()).toEqual([1])
  source.publish(2)
  expect(deserialized.currentValue()).toEqual([1, 2])
  source.publish(3)
  expect(deserialized.currentValue()).toEqual([1, 2])
})