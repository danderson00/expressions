const sum = require('../../../src/operators/stream/sum').component
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

test("sum accumulates total of stream values", () => {
  const source = subject()
  const handler = jest.fn()
  const observable = sum(source).create()
  observable.subscribe(handler)
  expect(observable.currentValue()).toBe(0)
  source.publish(1)
  expect(observable.currentValue()).toBe(1)
  source.publish(2)
  expect(observable.currentValue()).toBe(3)
  source.publish(3)
  expect(observable.currentValue()).toBe(6)
  expect(handler.mock.calls.map(x => x[0])).toEqual([1, 3, 6])
})

test("sum accumulates total of result of expression", () => {
  const source = subject()
  const handler = jest.fn()
  const observable = sum(source).create(x => x.value)
  observable.subscribe(handler)
  source.publish({ value: 1 })
  expect(observable.currentValue()).toBe(1)
  source.publish({ value: 2 })
  expect(observable.currentValue()).toBe(3)
  source.publish({ value: 3 })
  expect(observable.currentValue()).toBe(6)
  expect(handler.mock.calls.map(x => x[0])).toEqual([1, 3, 6])
})

test("sum of values can be serialized", () => {
  const source = subject()
  const observable = sum(source).create()
  source.publish(1)
  source.publish(2)

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = sum(source).unpack(parse(stringify(serializationInfo)))
  const handler = jest.fn()
  deserialized.subscribe(handler)

  expect(deserialized.currentValue()).toBe(3)
  source.publish(3)
  expect(deserialized.currentValue()).toBe(6)
  expect(handler.mock.calls.map(x => x[0])).toEqual([6])
})

test("sum of expression results can be serialized", () => {
  const source = subject()
  const observable = sum(source).create(x => x.value)
  source.publish({ value: 1 })
  source.publish({ value: 2 })

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = sum(source).unpack(parse(stringify(serializationInfo)))
  const handler = jest.fn()
  deserialized.subscribe(handler)

  expect(deserialized.currentValue()).toBe(3)
  source.publish({ value: 3 })
  expect(deserialized.currentValue()).toBe(6)
  expect(handler.mock.calls.map(x => x[0])).toEqual([6])
})

test("sum can be created from definition", () => {
  const source = subject()
  const definition = sum(source).create(x => x.value).getSerializationInfo().definition
  const deserialized = sum(source).unpack({ definition })
  const handler = jest.fn()
  deserialized.subscribe(handler)

  expect(deserialized.currentValue()).toBe(0)
  source.publish({ value: 1 })
  source.publish({ value: 2 })
  expect(deserialized.currentValue()).toBe(3)
})

test("sum can be serialized twice", () => {
  const source = subject()
  const observable = sum(source).create(x => x.value)
  source.publish({ value: 1 })

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = sum(source).unpack(parse(stringify(serializationInfo)))
  serializationInfo = deserialized.getSerializationInfo()
  deserialized = sum(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized.currentValue()).toBe(1)
  source.publish({ value: 3 })
  expect(deserialized.currentValue()).toBe(4)
})