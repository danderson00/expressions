const operator = require('../../../src/operators/stream/where')
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

const where = operator('where').component
const filter = operator('filter').component

test("where filters messages based on expression", () => {
  const source = subject()
  const handler = jest.fn()
  const observable = where(source).create(x => x % 2 === 1)
  observable.subscribe(handler)
  expect(observable.currentValue()).toBe(undefined)
  source.publish(1)
  expect(observable.currentValue()).toBe(1)
  source.publish(2)  
  expect(observable.currentValue()).toBe(1)
  source.publish(3)
  expect(observable.currentValue()).toBe(3)
  expect(handler.mock.calls.length).toEqual(2)
  expect(handler.mock.calls.map(x => x[0])).toEqual([1, 3])
})

test("where can be serialized", () => {
  const source = subject()
  const observable = where(source).create(x => x % 2 === 1)
  source.publish(1)

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = where(source).unpack(parse(stringify(serializationInfo)))
  const handler = jest.fn()
  deserialized.subscribe(handler)

  expect(deserialized.currentValue()).toBe(1)
  source.publish(3)
  expect(deserialized.currentValue()).toBe(3)
  expect(handler.mock.calls[0][0]).toEqual(3)  
})

test("where filters messages based on hash values", () => {
  const source = subject()
  const observable = where(source).create({ p1: 1, p2: 'test' })

  source.publish({ p1: 1, p2: 'test', p3: 10 })
  expect(observable.currentValue().p3).toBe(10)
  source.publish({ p1: 2, p2: 'test', p3: 20 })
  expect(observable.currentValue().p3).toBe(10)
  source.publish({ p1: 1, p2: 'test2', p3: 30 })
  expect(observable.currentValue().p3).toBe(10)
  source.publish({ p1: 1, p2: 'test', p3: 40 })
  expect(observable.currentValue().p3).toBe(40)
})

test("where with hash can be serialized", () => {
  const source = subject()
  const observable = where(source).create({ p1: 1, p2: 'test' })

  source.publish({ p1: 1, p2: 'test', p3: 10 })

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = where(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized.currentValue().p3).toBe(10)
  source.publish({ p1: 2, p2: 'test', p3: 20 })
  expect(deserialized.currentValue().p3).toBe(10)
  source.publish({ p1: 1, p2: 'test2', p3: 30 })
  expect(deserialized.currentValue().p3).toBe(10)
  source.publish({ p1: 1, p2: 'test', p3: 40 })
  expect(deserialized.currentValue().p3).toBe(40)
})

test("where can be created from definition", () => {
  const source = subject()
  const definition = where(source).create(x => x % 2 === 1).getSerializationInfo().definition
  const deserialized = where(source).unpack({ definition })
  const handler = jest.fn()
  deserialized.subscribe(handler)

  expect(deserialized.currentValue()).toBeUndefined()
  source.publish(3)
  source.publish(2)
  expect(deserialized.currentValue()).toBe(3)
})

test("where can be serialized twice", () => {  
  const source = subject()
  const observable = where(source).create(x => x % 2 === 1)
  source.publish(1)

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = where(source).unpack(parse(stringify(serializationInfo)))

  serializationInfo = deserialized.getSerializationInfo()
  deserialized = where(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized.currentValue()).toBe(1)
  source.publish(3)
  expect(deserialized.currentValue()).toBe(3)
})

test("filter can be serialized twice", () => {
  const source = subject()
  const observable = filter(source).create(x => x % 2 === 1)
  source.publish(1)

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = filter(source).unpack(parse(stringify(serializationInfo)))

  serializationInfo = deserialized.getSerializationInfo()
  deserialized = filter(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized.currentValue()).toBe(1)
  source.publish(3)
  expect(deserialized.currentValue()).toBe(3)
})
