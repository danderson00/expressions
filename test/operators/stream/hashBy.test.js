const hashBy = require('../../../src/operators/stream/groupBy')(true).component
const operators = require('../../../src/repository')(require('../../../src/operators'))
const { subject } = require('../../../src/observable')
const { unwrap } = require('../../../src')
const { stringify, parse } = require('../../utils')

test("hashBy creates a hash keyed by expression result", () => {
  const source = subject()
  const groups = hashBy(source, operators).create(x => x % 2)
  expect(unwrap(groups)).toEqual({})
  source.publish(1)
  expect(unwrap(groups)).toEqual({ 1: 1 })
  source.publish(2)
  expect(unwrap(groups)).toEqual({ 1: 1, 0: 2 })
  source.publish(3)
  expect(unwrap(groups)).toEqual({ 1: 3, 0: 2 })
})

test("hashBy creates a hash keyed by property name", () => {
  const source = subject()
  const groups = hashBy(source, operators).create('category')
  expect(unwrap(groups)).toEqual({})
  source.publish({ category: 1 })
  expect(unwrap(groups)).toEqual({ 1: { category: 1 } })
  source.publish({ category: 2 })
  expect(unwrap(groups)).toEqual({ 1: { category: 1 }, 2: { category: 2 } })
})

test("hashBy applies group expression to groups", () => {
  const source = subject()
  const groups = hashBy(source, operators).create(x => x % 2, o => o.map(x => x * 2))
  expect(unwrap(groups)).toEqual({})
  source.publish(1)
  expect(unwrap(groups)).toEqual({ 1: 2 })
  source.publish(2)
  expect(unwrap(groups)).toEqual({ 1: 2, 0: 4 })
  source.publish(3)
  expect(unwrap(groups)).toEqual({ 1: 6, 0: 4 })
})

test("hashBy can be serialized", () => {
  const source = subject()
  const groups = hashBy(source, operators).create(x => x % 2)
  source.publish(1)

  const serializationInfo = groups.getSerializationInfo()
  const deserialized = hashBy(source, operators).unpack(parse(stringify(serializationInfo)))

  expect(unwrap(deserialized)).toEqual({ 1: 1 })
  source.publish(2)
  expect(unwrap(deserialized)).toEqual({ 1: 1, 0: 2 })
  source.publish(3)
  expect(unwrap(deserialized)).toEqual({ 1: 3, 0: 2 })
})

test("hashBy can be created from definition", () => {
  const source = subject()
  const definition = hashBy(source, operators).create(x => x % 2).getSerializationInfo().definition
  const deserialized = hashBy(source, operators).unpack({ definition })

  expect(unwrap(deserialized)).toEqual({})
  source.publish(1)
  expect(unwrap(deserialized)).toEqual({ 1: 1 })
  source.publish(2)
  expect(unwrap(deserialized)).toEqual({ 1: 1, 0: 2 })
})

test("hashBy can be serialized twice", () => {  
  const source = subject()
  const groups = hashBy(source, operators).create(x => x % 2)
  source.publish(1)

  let serializationInfo = groups.getSerializationInfo()
  let deserialized = hashBy(source, operators).unpack(parse(stringify(serializationInfo)))
  serializationInfo = deserialized.getSerializationInfo()
  deserialized = hashBy(source, operators).unpack(parse(stringify(serializationInfo)))

  expect(unwrap(deserialized)).toEqual({ 1: 1 })
  source.publish(2)
  expect(unwrap(deserialized)).toEqual({ 1: 1, 0: 2 })
  source.publish(3)
  expect(unwrap(deserialized)).toEqual({ 1: 3, 0: 2 })
})