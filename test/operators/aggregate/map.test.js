const map = require('../../../src/operators/aggregate/map').component
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

test("map maps elements from aggregate", () => {
  const source = subject()
  const observable = map(source).create(x => x * 2)
  expect(observable()).toEqual([])
  source.publish([1])
  expect(observable()).toEqual([2])
  source.publish([1, 2])
  expect(observable()).toEqual([2, 4])
})

test("map can be serialized", () => {
  const source = subject()
  const observable = map(source).create(x => x * 2)
  source.publish([1])

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = map(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toEqual([2])
  source.publish([1, 2])
  expect(deserialized()).toEqual([2, 4])
})

test("map can be created from definition", () => {
  const source = subject()
  const definition = map(source).create(x => x * 2).getSerializationInfo().definition
  const deserialized = map(source).unpack({ definition })

  expect(deserialized()).toEqual([])
  source.publish([1, 2])
  expect(deserialized()).toEqual([2, 4])
})

test("map can be serialized twice", () => {
  const source = subject()
  const observable = map(source).create(x => x * 2)
  source.publish([1])

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = map(source).unpack(parse(stringify(serializationInfo)))
  serializationInfo = deserialized.getSerializationInfo()
  deserialized = map(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toEqual([2])
  source.publish([1, 2])
  expect(deserialized()).toEqual([2, 4])
})