const operator = require('../../../src/operators/aggregate/where')
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

const where = operator('where').component

test("where filters elements from aggregate", () => {
  const source = subject()
  const observable = where(source).create(x => x > 1)
  expect(observable()).toEqual([])
  source.publish([1, 2, 3])
  expect(observable()).toEqual([2, 3])
  source.publish([1, 2, 3, 1, 4, 5])
  expect(observable()).toEqual([2, 3, 4, 5])
})

test("where can be serialized", () => {
  const source = subject()
  const observable = where(source).create(x => x > 1)
  source.publish([1, 2, 3])

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = where(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toEqual([2, 3])
  source.publish([1, 2, 3, 1, 4, 5])
  expect(deserialized()).toEqual([2, 3, 4, 5])
})

test("where can be created from definition", () => {
  const source = subject()
  const definition = where(source).create(x => x > 1).getSerializationInfo().definition
  const deserialized = where(source).unpack({ definition })

  expect(deserialized()).toEqual([])
  source.publish([1, 2, 3])
  expect(deserialized()).toEqual([2, 3])
})

test("where can be serialized twice", () => {
  const source = subject()
  const observable = where(source).create(x => x > 1)
  source.publish([1, 2, 3])

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = where(source).unpack(parse(stringify(serializationInfo)))
  serializationInfo = deserialized.getSerializationInfo()
  deserialized = where(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toEqual([2, 3])
  source.publish([1, 2, 3, 1, 4, 5])
  expect(deserialized()).toEqual([2, 3, 4, 5])
})