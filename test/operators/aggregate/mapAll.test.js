const mapAll = require('../../../src/operators/aggregate/mapAll').component
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

test("mapAll maps entire aggregate", () => {
  const source = subject()
  const observable = mapAll(source).create(x => x.length)
  expect(observable()).toEqual(undefined)
  source.publish([1])
  expect(observable()).toEqual(1)
  source.publish([1, 2])
  expect(observable()).toEqual(2)
})

test("mapAll can be serialized", () => {
  const source = subject()
  const observable = mapAll(source).create(x => x.length)
  source.publish([1])

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = mapAll(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toEqual(1)
  source.publish([1, 2])
  expect(deserialized()).toEqual(2)
})

test("mapAll can be created from definition", () => {
  const source = subject()
  const definition = mapAll(source).create(x => x.length).getSerializationInfo().definition
  const deserialized = mapAll(source).unpack({ definition })

  expect(deserialized()).toEqual(undefined)
  source.publish([1, 2])
  expect(deserialized()).toEqual(2)
})

test("mapAll can be serialized twice", () => {
  const source = subject()
  const observable = mapAll(source).create(x => x.length)
  source.publish([1])

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = mapAll(source).unpack(parse(stringify(serializationInfo)))
  serializationInfo = deserialized.getSerializationInfo()
  deserialized = mapAll(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toEqual(1)
  source.publish([1, 2])
  expect(deserialized()).toEqual(2)
})