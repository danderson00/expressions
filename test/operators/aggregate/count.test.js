const count = require('../../../src/operators/aggregate/count').component
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

test("count returns number of elements in group", () => {
  const source = subject()
  const observable = count(source).create()
  expect(observable()).toBe(0)
  source.publish([1])
  expect(observable()).toBe(1)
  source.publish([1, 2])
  expect(observable()).toBe(2)
})

test("count can be serialized", () => {
  const source = subject()
  const observable = count(source).create()
  source.publish([1])

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = count(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toBe(1)
  source.publish([1, 2])
  expect(deserialized()).toBe(2)
})

test("count can be created from definition", () => {
  const source = subject()
  const definition = count(source).create().getSerializationInfo().definition
  const deserialized = count(source).unpack({ definition })

  expect(deserialized()).toBe(0)
  source.publish([1])
  expect(deserialized()).toBe(1)
  source.publish([1, 2])
  expect(deserialized()).toBe(2)
})

test("count can be serialized twice", () => {
  const source = subject()
  const observable = count(source).create()
  source.publish([1])

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = count(source).unpack(parse(stringify(serializationInfo)))
  serializationInfo = deserialized.getSerializationInfo()
  deserialized = count(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toBe(1)
  source.publish([1, 2])
  expect(deserialized()).toBe(2)
})