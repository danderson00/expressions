const average = require('../../../src/operators/aggregate/average').component
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

test("average returns average of elements in group", () => {
  const source = subject()
  const observable = average(source).create()
  expect(observable.currentValue()).toBe(0)
  source.publish([1])
  expect(observable.currentValue()).toBe(1)
  source.publish([1, 3])
  expect(observable.currentValue()).toBe(2)
})

test("average returns average of expression applied to elements in group", () => {
  const source = subject()
  const observable = average(source).create(x => x.value)
  expect(observable.currentValue()).toBe(0)
  source.publish([{ value: 1 }])
  expect(observable.currentValue()).toBe(1)
  source.publish([{ value: 1 }, { value: 3 }])
  expect(observable.currentValue()).toBe(2)
})

test("average can be serialized", () => {
  const source = subject()
  const observable = average(source).create()
  source.publish([1])

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = average(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized.currentValue()).toBe(1)
  source.publish([1, 3])
  expect(deserialized.currentValue()).toBe(2)
})

test("average with expression can be serialized", () => {
  const source = subject()
  const observable = average(source).create(x => x.value)
  source.publish([{ value: 1 }])

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = average(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized.currentValue()).toBe(1)
  source.publish([{ value: 1 }, { value: 3 }])
  expect(deserialized.currentValue()).toBe(2)
})

test("average can be created from definition", () => {
  const source = subject()
  const definition = average(source).create().getSerializationInfo().definition
  const deserialized = average(source).unpack({ definition })

  expect(deserialized.currentValue()).toBe(0)
  source.publish([1])
  expect(deserialized.currentValue()).toBe(1)
  source.publish([1, 3])
  expect(deserialized.currentValue()).toBe(2)
})

test("average can be serialized twice", () => {
  const source = subject()
  const observable = average(source).create()
  source.publish([1])

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = average(source).unpack(parse(stringify(serializationInfo)))
  serializationInfo = deserialized.getSerializationInfo()
  deserialized = average(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized.currentValue()).toBe(1)
  source.publish([1, 3])
  expect(deserialized.currentValue()).toBe(2)
})