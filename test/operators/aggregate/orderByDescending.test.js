const orderByDescending = require('../../../src/operators/aggregate/orderByDescending').component
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

test("orderByDescending orders elements by property name from aggregate", () => {
  const source = subject()
  const observable = orderByDescending(source).create('p1')
  expect(observable()).toEqual([])
  source.publish([{ p1: 3 }, { p1: 1 }])
  expect(observable()).toEqual([{ p1: 3 }, { p1: 1 }])
  source.publish([{ p1: 3 }, { p1: 1 }, { p1: 2 }])
  expect(observable()).toEqual([{ p1: 3 }, { p1: 2 }, { p1: 1 }])
})

test("orderByDescending orders elements by expression from aggregate", () => {
  const source = subject()
  const observable = orderByDescending(source).create(x => x.p1)
  expect(observable()).toEqual([])
  source.publish([{ p1: 3 }, { p1: 1 }])
  expect(observable()).toEqual([{ p1: 3 }, { p1: 1 }])
  source.publish([{ p1: 3 }, { p1: 1 }, { p1: 2 }])
  expect(observable()).toEqual([{ p1: 3 }, { p1: 2 }, { p1: 1 }])
})

test("orderByDescending can be serialized", () => {
  const source = subject()
  const observable = orderByDescending(source).create('p1')
  source.publish([{ p1: 3 }, { p1: 1 }])

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = orderByDescending(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toEqual([{ p1: 3 }, { p1: 1 }])
  source.publish([{ p1: 3 }, { p1: 1 }, { p1: 2 }])
  expect(deserialized()).toEqual([{ p1: 3 }, { p1: 2 }, { p1: 1 }])
})

test("orderByDescending can be created from definition", () => {
  const source = subject()
  const definition = orderByDescending(source).create('p1').getSerializationInfo().definition
  const deserialized = orderByDescending(source).unpack({ definition })

  expect(deserialized()).toEqual([])
  source.publish([{ p1: 3 }, { p1: 1 }])
  expect(deserialized()).toEqual([{ p1: 3 }, { p1: 1 }])
})

test("orderByDescending can be serialized twice", () => {
  const source = subject()
  const observable = orderByDescending(source).create('p1')
  source.publish([{ p1: 3 }, { p1: 1 }])

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = orderByDescending(source).unpack(parse(stringify(serializationInfo)))
  serializationInfo = observable.getSerializationInfo()
  deserialized = orderByDescending(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toEqual([{ p1: 3 }, { p1: 1 }])
  source.publish([{ p1: 3 }, { p1: 1 }, { p1: 2 }])
  expect(deserialized()).toEqual([{ p1: 3 }, { p1: 2 }, { p1: 1 }])
})