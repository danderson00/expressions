const orderBy = require('../../../src/operators/aggregate/orderBy').component
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

test("orderBy orders elements by property name from aggregate", () => {
  const source = subject()
  const observable = orderBy(source).create('p1')
  expect(observable()).toEqual([])
  source.publish([{ p1: 31 }, { p1: 11 }])
  expect(observable()).toEqual([{ p1: 11 }, { p1: 31 }])
  source.publish([{ p1: 31 }, { p1: 11 }, { p1: 41 }, { p1: 21 }])
  expect(observable()).toEqual([{ p1: 11 }, { p1: 21 }, { p1: 31 }, { p1: 41 }])
})

test("orderBy orders elements by expression from aggregate", () => {
  const source = subject()
  const observable = orderBy(source).create(x => x.p1)
  expect(observable()).toEqual([])
  source.publish([{ p1: 3 }, { p1: 1 }])
  expect(observable()).toEqual([{ p1: 1 }, { p1: 3 }])
  source.publish([{ p1: 3 }, { p1: 1 }, { p1: 2 }])
  expect(observable()).toEqual([{ p1: 1 }, { p1: 2 }, { p1: 3 }])
})

test("orderBy can be serialized", () => {
  const source = subject()
  const observable = orderBy(source).create('p1')
  source.publish([{ p1: 3 }, { p1: 1 }])

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = orderBy(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toEqual([{ p1: 1 }, { p1: 3 }])
  source.publish([{ p1: 3 }, { p1: 1 }, { p1: 2 }])
  expect(deserialized()).toEqual([{ p1: 1 }, { p1: 2 }, { p1: 3 }])
})

test("orderBy can be created from definition", () => {
  const source = subject()
  const definition = orderBy(source).create('p1').getSerializationInfo().definition
  const deserialized = orderBy(source).unpack({ definition })

  expect(deserialized()).toEqual([])
  source.publish([{ p1: 3 }, { p1: 1 }])
  expect(deserialized()).toEqual([{ p1: 1 }, { p1: 3 }])
})

test("orderBy can be serialized twice", () => {
  const source = subject()
  const observable = orderBy(source).create('p1')
  source.publish([{ p1: 3 }, { p1: 1 }])

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = orderBy(source).unpack(parse(stringify(serializationInfo)))
  serializationInfo = observable.getSerializationInfo()
  deserialized = orderBy(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toEqual([{ p1: 1 }, { p1: 3 }])
  source.publish([{ p1: 3 }, { p1: 1 }, { p1: 2 }])
  expect(deserialized()).toEqual([{ p1: 1 }, { p1: 2 }, { p1: 3 }])
})