const sum = require('../../../src/operators/aggregate/sum').component
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

test("sum returns sum of values in group", () => {
  const source = subject()
  const observable = sum(source).create()
  expect(observable.currentValue()).toBe(0)
  source.publish([1])
  expect(observable.currentValue()).toBe(1)
  source.publish([1, 3])
  expect(observable.currentValue()).toBe(4)
})

test("sum returns sum of expression applied to elements in group", () => {
  const source = subject()
  const observable = sum(source).create(x => x.value)
  expect(observable.currentValue()).toBe(0)
  source.publish([{ value: 1 }])
  expect(observable.currentValue()).toBe(1)
  source.publish([{ value: 1 }, { value: 3 }])
  expect(observable.currentValue()).toBe(4)
})

test("sum can be serialized", () => {
  const source = subject()
  const observable = sum(source).create()
  source.publish([1])

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = sum(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized.currentValue()).toBe(1)
  source.publish([1, 3])
  expect(deserialized.currentValue()).toBe(4)
})

test("sum with expression can be serialized", () => {
  const source = subject()
  const observable = sum(source).create(x => x.value)
  source.publish([{ value: 1 }])

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = sum(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized.currentValue()).toBe(1)
  source.publish([{ value: 1 }, { value: 3 }])
  expect(deserialized.currentValue()).toBe(4)
})

test("sum can be created from definition", () => {
  const source = subject()
  const definition = sum(source).create().getSerializationInfo().definition
  const deserialized = sum(source).unpack({ definition })

  expect(deserialized.currentValue()).toBe(0)
  source.publish([1])
  expect(deserialized.currentValue()).toBe(1)
  source.publish([1, 3])
  expect(deserialized.currentValue()).toBe(4)
})

test("sum can be serialized twice", () => {
  const source = subject()
  const observable = sum(source).create()
  source.publish([1])

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = sum(source).unpack(parse(stringify(serializationInfo)))
  serializationInfo = deserialized.getSerializationInfo()
  deserialized = sum(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized.currentValue()).toBe(1)
  source.publish([1, 3])
  expect(deserialized.currentValue()).toBe(4)
})