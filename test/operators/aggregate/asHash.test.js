const asHash = require('../../../src/operators/aggregate/asHash').component
const { subject } = require('../../../src/observable')
const { stringify, parse } = require('../../utils')

test("asHash projects values into object by key property", () => {
  const source = subject()
  const observable = asHash(source).create()
  expect(observable()).toEqual({})
  source.publish([{ key: 1 }])
  expect(observable()).toEqual({ 1: { key: 1 } })
  source.publish([{ key: 1 }, { key: 2 }])
  expect(observable()).toEqual({ 1: { key: 1 }, 2: { key: 2 } })
})

test("asHash can be serialized", () => {
  const source = subject()
  const observable = asHash(source).create()
  source.publish([{ key: 1 }])

  const serializationInfo = observable.getSerializationInfo()
  const deserialized = asHash(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toEqual({ 1: { key: 1 } })
  source.publish([{ key: 1 }, { key: 2 }])
  expect(deserialized()).toEqual({ 1: { key: 1 }, 2: { key: 2 } })
})

test("asHash can be created from definition", () => {
  const source = subject()
  const definition = asHash(source).create().getSerializationInfo().definition
  const deserialized = asHash(source).unpack({ definition })

  expect(deserialized()).toEqual({})
  source.publish([{ key: 1 }])
  expect(deserialized()).toEqual({ 1: { key: 1 } })
  source.publish([{ key: 1 }, { key: 2 }])
  expect(deserialized()).toEqual({ 1: { key: 1 }, 2: { key: 2 } })

})

test("asHash can be serialized twice", () => {
  const source = subject()
  const observable = asHash(source).create()
  source.publish([{ key: 1 }])

  let serializationInfo = observable.getSerializationInfo()
  let deserialized = asHash(source).unpack(parse(stringify(serializationInfo)))
  serializationInfo = observable.getSerializationInfo()
  deserialized = asHash(source).unpack(parse(stringify(serializationInfo)))

  expect(deserialized()).toEqual({ 1: { key: 1 } })
  source.publish([{ key: 1 }, { key: 2 }])
  expect(deserialized()).toEqual({ 1: { key: 1 }, 2: { key: 2 } })

})