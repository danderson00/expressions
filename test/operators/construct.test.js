const { subject, serialize, deserialize, construct } = require('../../src')

test("construct invokes specified component", () => {
  const source = subject()
  const o = source.construct('sum')
  source.publish(1)
  source.publish(2)
  expect(o()).toBe(3)
})

test("construct invokes specified component with parameters", () => {
  const source = subject()
  const o = source.construct('select', 'value')
  source.publish({ value: 'test' })
  expect(o()).toBe('test')
})

test("construct invokes vocabulary", () => {
  const expressions = construct()
  expressions.addVocabulary({ name: 'pluck', expression: (o, property) => o.select(property) })
  const source = expressions.subject()
  const o = source.construct('pluck', 'value')
  source.publish({ value: 'test' })
  expect(o()).toBe('test')
})

test("construct invokes aggregate operators", () => {
  const source = subject()
  const o = source.groupBy('category', o => o.sum('value')).construct('average')
  source.publish({ category: 1, value: 1 })
  source.publish({ category: 1, value: 2 })
  source.publish({ category: 2, value: 1 })
  expect(o()).toBe(2)
})

test("construct serializes", () => {
  const source = subject()
  const o = source.construct('select', 'value')
  source.publish({ value: 'test' })

  const source2 = subject()
  const serialized = serialize(o)
  const deserialized = deserialize(source2, serialized)

  expect(deserialized()).toBe('test')
  source2.publish({ value: 'test2' })
  expect(deserialized()).toBe('test2')
})

// TODO: unsure why these fail - for vocabulary constructed on the consumer, it works for now
// test("construct serializes vocabulary", () => {
//   const expressions = construct()
//   expressions.addVocabulary({ name: 'pluck', expression: o => o.select('value') })
//   const source = expressions.subject()
//   const o = source.construct('pluck')
//   source.publish({ value: 'test' })
//
//   const source2 = expressions.subject()
//   const serialized = expressions.serialize(o)
//   const deserialized = expressions.deserialize(source2, serialized)
//
//   expect(deserialized()).toBe('test')
//   source2.publish({ value: 'test2' })
//   expect(deserialized()).toBe('test2')
// })

test("construct serializes parameterised vocabulary", () => {
  const expressions = construct()
  expressions.addVocabulary({ name: 'pluck', expression: (o, property) => o.select(property) })
  const source = expressions.subject()
  const o = source.construct('pluck', 'value')
  source.publish({ value: 'test' })

  const source2 = expressions.subject()
  const serialized = expressions.serialize(o)
  const deserialized = expressions.deserialize(source2, serialized)

  expect(deserialized()).toBe('test')
  source2.publish({ value: 'test2' })
  expect(deserialized()).toBe('test2')
})

test("construct serializes aggregate operators", () => {
  const source = subject()
  const o = source.groupBy('category', o => o.sum('value')).construct('average')
  source.publish({ category: 1, value: 1 })

  const source2 = subject()
  const serialized = serialize(o)
  const deserialized = deserialize(source2, serialized)

  source2.publish({ category: 1, value: 2 })
  source2.publish({ category: 2, value: 1 })

  expect(deserialized()).toBe(2)
})
