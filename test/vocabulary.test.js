const { construct } = require('../src')

test("observables are extended with vocabulary", () => {
  const expressions = construct()
  const source = expressions.subject()
  expressions.addVocabulary({ name: 'sumOfEvens', expression: o => o.where(x => x % 2 === 0).sum() })
  const o = expressions(source).sumOfEvens()
  
  expect(o()).toBe(0)
  source.publish(1)
  expect(o()).toBe(0)
  source.publish(2)
  expect(o()).toBe(2)
  source.publish(3)
  expect(o()).toBe(2)
  source.publish(4)
  expect(o()).toBe(6)
})

test("vocabulary can be composed", () => {
  const expressions = construct()
  const source = expressions.subject()
  expressions.addVocabulary({ name: 'total', expression: o => o.sum(x => x.value) })
  expressions.addVocabulary({ name: 'count', expression: o => o.sum(() => 1) })
  expressions.addVocabulary({ name: 'average', expression: o => o.compose(
    o => o.total(),
    o => o.count(),
    (total, count) => total / count
  )})
  const o = expressions(source).average()

  expect(o()).toBe(NaN)
  source.publish({ value: 1 })
  expect(o()).toBe(1)
  source.publish({ value: 3 })
  expect(o()).toBe(2)
  source.publish({ value: 5 })
  expect(o()).toBe(3)
  source.publish({ value: 1 })
  expect(o()).toBe(2.5)
})

test("vocabulary can be parameterized", () => {
  const expressions = construct()
  expressions.addVocabulary({ name: 'multiplesOf', expression: (o, multiple) => o.where(x => x % multiple === 0) })
  const source = expressions.subject()
  const o = source.multiplesOf(3)

  source.publish(1)
  expect(o()).toBeUndefined()
  source.publish(6)
  expect(o()).toBe(6)
  source.publish(7)
  expect(o()).toBe(6)
})

test("vocabulary can be serialized", () => {
  const expressions = construct()
  const source = expressions.subject()
  expressions.addVocabulary({ name: 'sumOfEvens', expression: o => o.where(x => x % 2 === 0).sum() })
  const o = expressions(source).sumOfEvens()

  source.publish(2)

  const json = JSON.stringify(expressions.serialize(o))
  const deserialized = expressions.deserialize(source, JSON.parse(json))

  expect(deserialized()).toBe(2)
  source.publish(3)
  expect(deserialized()).toBe(2)
  source.publish(4)
  expect(deserialized()).toBe(6)
})

test("parameterized vocabulary can be serialized", () => {
  const expressions = construct()
  expressions.addVocabulary({ name: 'multiplesOf', expression: (o, multiple) => o.where(x => x % multiple === 0) })
  const source = expressions.subject()
  const o = source.multiplesOf(3)

  source.publish(3)

  const json = JSON.stringify(expressions.serialize(o))
  const deserializedSource = expressions.subject()
  const deserialized = expressions.deserialize(deserializedSource, JSON.parse(json))

  expect(deserialized()).toBe(3)
  deserializedSource.publish(4)
  expect(deserialized()).toBe(3)
  deserializedSource.publish(6)
  expect(deserialized()).toBe(6)
})

test("vocabulary can specify a target type", () => {
  const expressions = construct()
  expressions.addVocabulary({ name: 'numberOfGroups', type: 'aggregate', expression: o => o.count() })
  const source = expressions.subject()

  expect(source.numberOfGroups).toBeUndefined()

  const o = source.groupBy('category').numberOfGroups()

  source.publish({ category: 1 })
  expect(o()).toBe(1)
  source.publish({ category: 2 })
  expect(o()).toBe(2)
})

test("vocabulary can specify a return type", () => {
  const expressions = construct()
  expressions.addVocabulary({ name: 'group', expression: o => o.groupBy('category'), returns: 'categories' })
  expressions.addVocabulary({ name: 'groupCount', type: 'categories', expression: o => o.count() })
  const source = expressions.subject()

  expect(source.groupCount).toBeUndefined()

  const o = source.group().groupCount()

  source.publish({ category: 1 })
  expect(o()).toBe(1)
  source.publish({ category: 2 })
  expect(o()).toBe(2)
})

test("deserialized vocabulary contains the correct number of stack frames", () => {
  const expressions = construct()
  const source = expressions.subject()
  const expression = o => o.where(x => x % 2 === 0).sum()
  const definitionLength = expressions.extractDefinition(expression).length
  expressions.addVocabulary({ name: 'sumOfEvens', expression })
  const o = expressions(source).sumOfEvens()

  source.publish(2)

  const json = JSON.stringify(expressions.serialize(o))
  const deserialized = expressions.deserialize(source, JSON.parse(json))
  const state = deserialized.getSerializationInfo().state
  expect(state.frames.length).toBe(definitionLength)
})

test("vocabulary with preceding operators can be serialized", () => {
  const expressions = construct()
  const source = expressions.subject()
  expressions.addVocabulary({ name: 'double', expression: o => o.map(x => x * 2) })
  const o = expressions(source).where(x => x % 2 === 0).double()

  source.publish(2)

  const json = JSON.stringify(expressions.serialize(o))
  const deserialized = expressions.deserialize(source, JSON.parse(json))

  expect(deserialized()).toBe(4)
  source.publish(3)
  expect(deserialized()).toBe(4)
  source.publish(4)
  expect(deserialized()).toBe(8)
})

test("vocabulary handles return type correctly", () => {
  const expressions = construct()
  const source = expressions.subject()
  expressions.addVocabulary({ name: 'grouped', expression: o => o.groupBy('category', o => o.sum('value')) })
  expressions.addVocabulary({ name: 'summed', expression: o => o.grouped().mapAll(x => x.reduce((a, b) => a + b)) })
  const o = expressions(source).summed()

  source.publish({ category: 1, value: 2 })
  source.publish({ category: 1, value: 4 })
  source.publish({ category: 2, value: 4 })

  expect(o()).toBe(10)

  const json = JSON.stringify(expressions.serialize(o))
  const deserialized = expressions.deserialize(source, JSON.parse(json))

  expect(deserialized()).toBe(10)
  source.publish({ category: 2, value: 4 })
  expect(deserialized()).toBe(14)
})

test("vocabulary returns correct return type", () => {
  const expressions = construct()
  expressions.addVocabulary({ name: 'grouped', expression: o => o.groupBy('category', o => o.sum('value')) })
  const o = expressions(expressions.subject()).grouped()
  expect(o.getSerializationInfo().definition.returns).toBe('aggregate')
})