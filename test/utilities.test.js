const { flat, uid } = require('../src/utilities')

test("uid generates sequential integers from 1", () => {
  expect(uid()).toBe(1)
  expect(uid()).toBe(2)
  expect(uid()).toBe(3)
})

test("flat flattens arrays", () => {
  expect(flat([1, 2, [3], [[4, [5], 6], 7]])).toEqual([1, 2, 3, 4, 5, 6, 7])
})

test("flat removes undefined elements", () => {
  expect(flat([1, undefined, 0, 1, [2], [undefined]])).toEqual([1, 0, 1, 2])
  expect(flat()).toEqual([])
})