const { serialize } = require('../../src/serialization/observable')
const { subject, proxy, clone } = require('../../src')

test("serialization stops at and omits observable with source option", () => {
  const root = subject()
  const child1 = proxy(root, { source: true })
  const leaf = child1.where(x => x)

  expect(serialize(leaf).frames.length).toBe(1)
})

test("clone creates copy of observable with new parent", () => {
  const source = subject()
  const expression = source.where(x => x % 2).map(x => x * 2)

  source.publish(1)

  const cloned = clone(source, expression)
  expect(expression()).toBe(cloned())

  source.publish(2)
  expect(expression()).toBe(cloned())

  source.publish(3)
  expect(expression()).toBe(cloned())
})

test("serialize with stateOnly option only returns stack of state", () => {
  const source = subject()
  const expression = source.where(x => x % 2).map(x => x * 2)
  source.publish(1)
  expect(serialize(expression, true)).toEqual({
    frames: [
      { currentValue: 1 },
      { currentValue: 2 }
    ]
  })
})