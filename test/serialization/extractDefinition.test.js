const { extractDefinition, extractReturnType } = require('../../src')

test("extractDefinition passes parameters to expression", () => {
  extractDefinition((o, p1, p2) => {
    expect(p1).toBe('test')
    expect(p2).toBe(2)
  }, { parameters: ['test', 2] })
})

test("extractReturnType returns stream for stream types", () => {
  expect(extractReturnType(o => o)).toBe('stream')
  expect(extractReturnType(o => o.where(x => x == 1).sum())).toBe('stream')
})

test("extractReturnType returns aggregate for aggregate types", () => {
  expect(extractReturnType(o => o.all())).toBe('aggregate')
  expect(extractReturnType(o => o.groupBy('p1'))).toBe('aggregate')
})
