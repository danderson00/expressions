const { serialize, deserialize } = require('../../src/serialization/definition')

test("basic serialize / deserialize", () => {
  const serialized = serialize({
    number: 10,
    object: { p1: 1 },
    undef: undefined,
    nullValue: null,
    func: x => x * 2
  })

  const parsed = JSON.parse(JSON.stringify(serialized))
  const result = deserialize(parsed)

  expect(result.number).toBe(10)
  expect(result.object).toEqual({ p1: 1 })
  expect(result.undef).toBe(undefined)
  expect(result.nullValue).toBe(null)
  expect(result.func(2)).toBe(4)
})

test("undefined deserializes to empty object", () => {
  expect(deserialize(serialize(undefined))).toEqual({})
})

test("arrays of functions can be serialized", () => {
  const serialized = serialize({
    funcs: [x => x * 2, x => x + 1]
  })

  const parsed = JSON.parse(JSON.stringify(serialized))
  const result = deserialize(parsed)

  expect(result.funcs[0](2)).toBe(4)
  expect(result.funcs[1](2)).toBe(3)
})

test("closures are applied to expressions", () => {
  const serialized = serialize({ func: () => closure })

  const parsed = JSON.parse(JSON.stringify(serialized))
  const result = deserialize(parsed, { closures: { closure: 1 } })

  expect(result.func()).toBe(1)
})