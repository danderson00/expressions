const { getExecutionContext } = require('../../src/serialization/executionContext')

test("getExecutionContext walks expression tree", () => {
  const o = {
    getSerializationInfo: () => ({ definition: { executionContext: { closures: { a: 2 } } } }),
    parent: {
      getSerializationInfo: () => ({ }),
      parent: {
        getSerializationInfo: () => ({ definition: { executionContext: { closures: { a: 1, b: 2 } } } }),
      }
    }
  }
  expect(getExecutionContext(o)).toEqual({
    closures: { a: 2, b: 2 }
  })
})