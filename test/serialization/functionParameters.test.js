const { getFunctionParameters } = require('../../src/serialization/functionParameters')

const assert = (target, result) => expect(getFunctionParameters(target)).toEqual(result)

test("getFunctionParameters", () => {
  assert(getFunctionParameters, ['target'])

  // friggin' babel interfering with shit. pass strings, tests the same
  assert('()=>{}', [])
  assert('a=>{}', ['a'])
  assert('a => {}', ['a'])
  assert('(a,b)=>{}', ['a', 'b'])
  assert('(a, b) => {}', ['a', 'b'])
  assert('(a,\nb) => {}', ['a', 'b'])
  assert('(a, b = 1) => {}', ['a', 'b'])
  assert('(a, b, ...args) => {}', ['a', 'b', '...args'])

  assert('function(a, b, ...args) {}', ['a', 'b', '...args'])
  assert('function(a, b = 1, c = 4 * (5 / 3), d = 2) {}', ['a', 'b', 'c', 'd'])
  assert('async function(a, b, ...args) {}', ['a', 'b', '...args'])
  assert('function async(a, b, ...args) {}', ['a', 'b', '...args'])
  assert('function test(\n//test\na,b) {}', ['a', 'b'])
  assert('function(a, b, c) {}', ['a', 'b', 'c'])
  assert('function() {}', [])
  assert('function named(a, b, c) {}', ['a', 'b', 'c'])
  assert('function(a /* = 1 */, b /* = true */) {}', ['a', 'b'])
  assert('function fprintf(handle, fmt /*, ...*/) {}', ['handle', 'fmt'])
  assert('function(a, b = 1, c) {}', ['a', 'b', 'c'])
  assert('function(a = 4 * (5 / 3), b) {}', ['a', 'b'])
  assert('function (a /* fooled you*/) {}', ['a'])
  assert('function (a /* function() yes */, \n /* no, */b)/* omg! */ {}', ['a', 'b'])
  assert('function ( A, b \n,c ,d \n ) \n {}', ['A', 'b', 'c', 'd'])
  assert('function(a, b) {}', ['a', 'b'])
  assert('function $args(func) {}', ['func'])
  assert('function Object() {}', [])

  assert('category => category\n' +
    '      .groupBy(x => x.complete, group => group\n' +
    '        .sum(x => x.value)\n' +
    '      )\n' +
    '    ', ['category'])

  assert('fun2(a = 5*6/3, // Comment \n    b){ }', ['a', 'b'])

  assert('(a, /* \n' +
    '    */\n' +
    '    b, //commment \n' +
    '    c) => /** */{ }', ['a', 'b', 'c'])
})
