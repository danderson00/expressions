const frames = require('../../src/serialization/frames')
const { subject } = require('../../src/observable')
const expressions = require('../../src')
const { serialize, deserialize } = require('../../src/serialization')
const operators = require('../../src/repository')(require('../../src/operators'))

test("stack can be unzipped and zipped", () => {
  const source = subject()
  const observable = expressions.createExpression(source).where(x => x % 2 === 0).sum()
  source.publish(2)

  const stack = serialize(observable).frames
  const unzipped = frames.unzip(stack)

  expect(unzipped.definition.length).toBe(2)
  expect(unzipped.state.length).toBe(2)

  const zipped = frames.zip(unzipped.definition, unzipped.state)
  const deserialized = deserialize(operators, source, { frames: zipped })

  expect(deserialized.currentValue()).toBe(2)
  source.publish(4)
  expect(deserialized.currentValue()).toBe(6)
  source.publish(5)
  expect(deserialized.currentValue()).toBe(6)
})

test("stack can be unzipped and zipped using API", () => {
  const source = subject()
  const observable = expressions.createExpression(source).where(x => x % 2 === 0).sum()
  source.publish(2)

  const unzipped = expressions.unzip(observable)
  const deserialized = expressions.zip(source, unzipped.definition, unzipped.state)

  expect(deserialized.currentValue()).toBe(2)
  source.publish(4)
  expect(deserialized.currentValue()).toBe(6)
  source.publish(5)
  expect(deserialized.currentValue()).toBe(6)
})  