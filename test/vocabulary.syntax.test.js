const { construct, unwrap } = require('../src')

test("vocabulary can consist of a hash of expressions", () => {
  const expressions = construct()
  expressions.addVocabulary({
    sumOfValues: o => o.sum('value') 
  })
  const source = expressions.subject()
  const o = source.sumOfValues()
  
  source.publish({ value: 1 })
  expect(o()).toBe(1)
  source.publish({ value: 2 })
  expect(o()).toBe(3)
})

test("vocabulary can consist of a hash of child vocabularies", () => {
  const expressions = construct()
  expressions.addVocabulary({
    item: [
      o => o.where(x => x.topic === 'item'),
      {
        list: o => o.groupBy('itemId'),
        details: o => o.accumulate()
      }
    ]
  })
  const source = expressions.subject()
  const list = source.item().list()
  // usually an accumulate like this would be scoped by itemId, but we're just testing
  const details = source.item().details()
  
  source.publish({ topic: 'item', itemId: 1, name: 'test1' })
  source.publish({ topic: 'item', itemId: 2, value: 'test2' })
  source.publish({ topic: 'otherItem', itemId: 3, name: 'test2' })

  expect(unwrap(list)).toEqual([
    { topic: 'item', itemId: 1, name: 'test1' },
    { topic: 'item', itemId: 2, value: 'test2' }
  ])

  expect(unwrap(details)).toEqual({
    topic: 'item',
    itemId: 2,
    name: 'test1',
    value: 'test2'
  })
})
