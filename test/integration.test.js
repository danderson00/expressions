const { subject, extractDefinition, zip, unzip, serializable } = require('../src')
const expressions = require('../src')
const { serialize, deserialize } = require('../src/serialization')
const operators = require('../src/repository')(require('../src/operators'))
const { unwrap } = require('../src/utilities')

test("simple expression", () => {
  const source = subject()
  const observable = source.where(x => x % 2 === 0).sum()
  source.publish(2)

  const json = JSON.stringify(serialize(observable))
  const deserialized = deserialize(operators, source, JSON.parse(json))

  expect(deserialized()).toBe(2)
  source.publish(4)
  expect(deserialized()).toBe(6)
  source.publish(5)
  expect(deserialized()).toBe(6)
})

test("order dependent expression", () => {
  const source = subject()
  const observable = source.where(x => x.value % 2 === 0).map(x => x.value).sum()
  source.publish({ value: 2 })

  const json = JSON.stringify(serialize(observable))
  const deserialized = deserialize(operators, source, JSON.parse(json))

  expect(deserialized()).toBe(2)
  source.publish({ value: 4 })
  expect(deserialized()).toBe(6)
  source.publish({ value: 5 })
  expect(deserialized()).toBe(6)
})

test("complex groupBy expression", () => {
  const source = subject()
  const publish = (complete, category, AUD) => source.publish({ complete, category, value: { AUD } })
  const o = source
    .where(x => x.complete)
    .groupBy(x => x.category, group => group
      .map(x => x.value)
      .sum(x => x.AUD))
  
  publish(true, 'cat1', 1)
  publish(false, 'cat2', 1)

  expect(o().map(x => x())).toEqual([1])
  expect(o().map(x => x.key)).toEqual(['cat1'])

  const json = JSON.stringify(serialize(o))
  const d = deserialize(operators, source, JSON.parse(json))

  expect(d().map(x => x())).toEqual([1])
  expect(d().map(x => x.key)).toEqual(['cat1'])

  publish(true, 'cat2', 2)
  expect(d().map(x => x())).toEqual([1, 2])

  publish(false, 'cat1', 2)
  publish(false, 'cat3', 2)
  publish(true, 'cat2', 2)

  expect(d().map(x => x())).toEqual([1, 4])
  expect(d().map(x => x.key)).toEqual(['cat1', 'cat2'])
})

test("nested groupBy expression", () => {
  const source = subject()
  const publish = (category, complete, value) => source.publish({ category, complete, value })
  const o = source
    .groupBy(x => x.category, category => category
      .groupBy(x => x.complete, group => group
        .sum(x => x.value)
      )
    )
  publish('cat1', true, 1)
  publish('cat1', true, 2)
  publish('cat1', false, 2)
  publish('cat2', true, 3)
  publish('cat2', false, 4)

  expect(o().map(x => x().map(y => y()))).toEqual([[3, 2], [3, 4]])
  expect(o().map(x => x.key)).toEqual(['cat1', 'cat2'])
  expect(o().map(x => x().map(y => y.key))).toEqual([[true, false], [true, false]])

  let json = JSON.stringify(serialize(o))
  let d = deserialize(operators, source, JSON.parse(json))
  json = JSON.stringify(serialize(d))
  d = deserialize(operators, source, JSON.parse(json))

  expect(d().map(x => x().map(y => y()))).toEqual([[3, 2], [3, 4]])
  expect(d().map(x => x.key)).toEqual(['cat1', 'cat2'])
  expect(d().map(x => x().map(y => y.key))).toEqual([[true, false], [true, false]])

  publish('cat1', true, 1)
  publish('cat1', false, 2)
  publish('cat2', true, 3)
  publish('cat2', false, 4)
  publish('cat3', true, 5)

  expect(d().map(x => x().map(y => y()))).toEqual([[4, 4], [6, 8], [5]])
  expect(d().map(x => x.key)).toEqual(['cat1', 'cat2', 'cat3'])
  expect(d().map(x => x().map(y => y.key))).toEqual([[true, false], [true, false], [true]])
})

test("simple vocabulary", () => {
  const t = expressions.construct()
  t.addVocabulary({
    name: 'total',
    expression: o => o.sum(x => x.value)
  })
  const source = t.subject()
  const o = source.total()

  source.publish({ value: 2 })
  expect(o()).toBe(2)

  const json = JSON.stringify(t.serialize(o))
  const d = t.deserialize(source, JSON.parse(json))

  expect(d()).toBe(2)
  source.publish({ value: 3 })
  expect(o()).toBe(5)
})

test("compose", () => {
  const t = expressions.construct()
  const source = t.subject()

  const o = source.compose(
    o => o.sum(x => x.score),
    o => o.sum(() => 1),
    (total, count) => ({ total: total, count: count })
  )

  source.publish({ score: 10 })
  expect(unwrap(o)).toEqual({ total: 10, count: 1 })

  const json = JSON.stringify(t.serialize(o))
  const d = t.deserialize(source, JSON.parse(json))

  expect(unwrap(d)).toEqual({ total: 10, count: 1 })
  source.publish({ score: 15 })
  expect(unwrap(d)).toEqual({ total: 25, count: 2 })
})

test("groupBy with compose", () => {
  const t = expressions.construct()
  const source = t.subject()

  const o = source
    .groupBy(x => x.player, player => player
      .compose(
        o => o.sum(x => x.score),
        o => o.sum(() => 1),
        (total, count) => ({ total: total, count: count })
      )
    )

  source.publish({ player: 'Jack', score: 10 })
  source.publish({ player: 'Jill', score: 12 })
  source.publish({ player: 'Jack', score: 15 })

  expect(unwrap(o)).toEqual([
    { total: 25, count: 2 },
    { total: 12, count: 1 }
  ])

  const json = JSON.stringify(t.serialize(o))
  const d = t.deserialize(source, JSON.parse(json))

  expect(unwrap(d)).toEqual([
    { total: 25, count: 2 },
    { total: 12, count: 1 }
  ])

  source.publish({ player: 'Jane', score: 20 })
  source.publish({ player: 'Jill', score: 14 })
  source.publish({ player: 'Jack', score: 20 })
  source.publish({ player: 'Jill', score: 13 })

  expect(unwrap(d)).toEqual([
    { total: 45, count: 3 },
    { total: 39, count: 3 },
    { total: 20, count: 1 }
  ])
})

test("groupBy with vocabulary and compose", () => {
  const t = expressions.construct()
  t.addVocabulary({
    name: 'totalScore',
    expression: o => o.sum(x => x.score)
  })
  t.addVocabulary({
    name: 'averageScore',
    expression: o => o.compose(
      o => o.totalScore(),
      o => o.sum(() => 1),
      (total, count) => total / count
    )
  })
  const source = t.subject()

  const o = source
    .groupBy(x => x.player, player => player
      .compose(
        o => o.averageScore(),
        o => o.totalScore(),
        (average, total) => ({ average, total })
      )
    )

  source.publish({ player: 'Jack', score: 10 })
  source.publish({ player: 'Jill', score: 12 })
  source.publish({ player: 'Jack', score: 15 })

  expect(unwrap(o)).toEqual([
    { average: 12.5, total: 25 },
    { average: 12, total: 12 }
  ])

  const json = JSON.stringify(t.serialize(o))
  const d = t.deserialize(source, JSON.parse(json))

  expect(unwrap(d)).toEqual([
    { average: 12.5, total: 25 },
    { average: 12, total: 12 }
  ])

  source.publish({ player: 'Jane', score: 20 })
  source.publish({ player: 'Jill', score: 14 })
  source.publish({ player: 'Jack', score: 20 })
  source.publish({ player: 'Jill', score: 13 })

  expect(unwrap(d)).toEqual([
    { average: 15, total: 45 },
    { average: 13, total: 39 },
    { average: 20, total: 20 }
  ])
})

test("closures can be used in expressions", () => {
  const categoryId = 1
  const source = serializable.subject({ executionContext: { closures: { categoryId } }, source: false })
  const observable = source.where(x => x.categoryId === categoryId).sum('value')
  source.publish({ categoryId: 1, value: 1 })
  expect(observable()).toBe(1)

  const deserializedSource = subject()
  let json = JSON.stringify(serialize(observable))
  let deserialized = deserialize(operators, deserializedSource, JSON.parse(json))

  expect(deserialized()).toBe(1)
  deserializedSource.publish({ categoryId: 2, value: 1 })
  expect(deserialized()).toBe(1)
  deserializedSource.publish({ categoryId: 1, value: 2 })
  expect(deserialized()).toBe(3)

  json = JSON.stringify(serialize(deserialized))
  deserialized = deserialize(operators, deserializedSource, JSON.parse(json))

  expect(deserialized()).toBe(3)
  deserializedSource.publish({ categoryId: 2, value: 1 })
  expect(deserialized()).toBe(3)
  deserializedSource.publish({ categoryId: 1, value: 3 })
  expect(deserialized()).toBe(6)
})

test("closures can be used with extractDefinition", () => {
  const categoryId = 1
  const definition = extractDefinition(
    o => o.where(x => x.categoryId === categoryId).sum('value'),
    { executionContext: { closures: { categoryId } }, source: false }
  )
  const source = subject()
  const deserialized = zip(source, definition)
  
  source.publish({ categoryId: 1, value: 1 })
  expect(deserialized()).toBe(1)
  source.publish({ categoryId: 2, value: 1 })
  expect(deserialized()).toBe(1)
  source.publish({ categoryId: 1, value: 2 })
  expect(deserialized()).toBe(3)
})

test("embedded parameterised vocabulary", () => {
  const t = expressions.construct()
  t.addVocabulary({
    name: 'scoresAbove',
    expression: (o, minimumScore) => o.where(x => x.score > minimumScore).count()
  })
  t.addVocabulary({
    name: 'totalScore',
    expression: o => o.sum(x => x.score)
  })
  const source = t.subject()

  const o = source
    .groupBy(x => x.player, player => player
      .assign({
        total: o => o.totalScore(),
        aboveSix: o => o.scoresAbove(6)
      })
    )

  source.publish({ player: 'Jack', score: 6 })
  source.publish({ player: 'Jill', score: 12 })
  source.publish({ player: 'Jack', score: 15 })

  expect(unwrap(o)).toEqual([
    { total: 21, aboveSix: 1 },
    { total: 12, aboveSix: 1 }
  ])

  const json = JSON.stringify(t.serialize(o))
  const d = t.deserialize(source, JSON.parse(json))

  expect(unwrap(d)).toEqual([
    { total: 21, aboveSix: 1 },
    { total: 12, aboveSix: 1 }
  ])

  source.publish({ player: 'Jane', score: 20 })
  source.publish({ player: 'Jill', score: 14 })
  source.publish({ player: 'Jack', score: 2 })
  source.publish({ player: 'Jill', score: 13 })

  expect(unwrap(d)).toEqual([
    { total: 23, aboveSix: 1 },
    { total: 39, aboveSix: 3 },
    { total: 20, aboveSix: 1 }
  ])
})

test("deserialize nested groupBy from definition and serialized state", () => {
  const source = subject()
  const publish = (category, complete, value) => source.publish({ category, complete, value })
  const o = source
    .groupBy(x => x.category, category => category
      .groupBy(x => x.complete, group => group
        .sum(x => x.value)
      )
    )
  const definition = unzip(o).definition

  publish('cat1', true, 1)
  publish('cat1', true, 2)
  publish('cat1', false, 2)
  publish('cat2', true, 3)
  publish('cat2', false, 4)

  const state1 = unzip(o).state
  const d1 = zip(source, definition, state1)
  const state2 = unzip(d1).state
  const d2 = zip(source, definition, state2)

  expect(unwrap(d2)).toEqual([[3, 2], [3, 4]])
  expect(d2().map(x => x.key)).toEqual(['cat1', 'cat2'])
  expect(d2().map(x => x().map(y => y.key))).toEqual([[true, false], [true, false]])

  publish('cat1', true, 1)
  publish('cat1', false, 2)
  publish('cat2', true, 3)
  publish('cat2', false, 4)
  publish('cat3', true, 5)

  expect(unwrap(d1)).toEqual([[4, 4], [6, 8], [5]])
  expect(d2().map(x => x.key)).toEqual(['cat1', 'cat2', 'cat3'])
  expect(d2().map(x => x().map(y => y.key))).toEqual([[true, false], [true, false], [true]])
})

test("groupBy with removeExpression from definition and serialized state", () => {
  const source = subject()
  const expression = o => o.groupBy('userId',
    o => o.select('liked'),
    o => o.where(x => {
      return x.liked === false
    })
  ).count()

  const definition = extractDefinition(expression)
  const o = expression(source)

  source.publish({ userId: 1, liked: true })
  source.publish({ userId: 2, liked: true })
  source.publish({ userId: 1, liked: false })

  const state1 = unzip(o).state
  const d1 = zip(source, definition, state1)
  const state2 = unzip(d1).state
  const d2 = zip(source, definition, state2)

  expect(unwrap(d2)).toEqual(1)

  source.publish({ userId: 3, liked: true })
  source.publish({ userId: 2, liked: false })

  expect(unwrap(d2)).toEqual(1)
})

test("deserialize groupBy with compose from definition and serialized state", () => {
  const source = subject()
  const expression = o => o.where(x => x.reviewId).groupBy('reviewId',
    o => o.compose(
      o => o.topic('review').accumulate(),
      o => o.topic('like').groupBy('userId',
        o => o.select('liked'),
        o => o.where(x => x.liked === false)
      ).count(),
      (details, likes) => ({ ...details, likes })
    )
  )
  const definition = extractDefinition(expression)
  const o = expression(source)

  source.publish({ topic: 'somethingelse' })
  source.publish({ topic: 'review', reviewId: '1', text: 'test1' })
  source.publish({ topic: 'like', reviewId: '1', userId: '1', liked: true })
  source.publish({ topic: 'like', reviewId: '1', userId: '2', liked: true })
  source.publish({ topic: 'like', reviewId: '1', userId: '3', liked: true })
  source.publish({ topic: 'like', reviewId: '1', userId: '1', liked: false })
  source.publish({ topic: 'review', reviewId: '2', text: 'test2' })
  source.publish({ topic: 'like', reviewId: '2', userId: '1', liked: true })
 
  const state1 = unzip(o).state
  const d1 = zip(source, definition, state1)
  const state2 = unzip(d1).state
  const d2 = zip(source, definition, state2)

  expect(unwrap(d2)).toEqual([
    { topic: 'review', reviewId: '1', text: 'test1', likes: 2 },
    { topic: 'review', reviewId: '2', text: 'test2', likes: 1 }
  ])

  source.publish({ topic: 'review', reviewId: '3', text: 'test3' })
  source.publish({ topic: 'like', reviewId: '1', userId: '2', liked: false })

  expect(unwrap(d2)).toEqual([
    { topic: 'review', reviewId: '1', text: 'test1', likes: 1 },
    { topic: 'review', reviewId: '2', text: 'test2', likes: 1 },
    { topic: 'review', reviewId: '3', text: 'test3', likes: 0 }
  ])
})

test("aggregate return type is deserialized correctly", () => {
  const o = subject().all()
  const json = JSON.stringify(expressions.serialize(o))
  const deserialized = expressions.deserialize(subject(), JSON.parse(json))
  expect(deserialized.getSerializationInfo().definition.returns).toBe('aggregate')
})

test("tap executes sink function without side effects", () => {
  const source = subject()
  const sink = jest.fn()
  const handler = jest.fn()
  source
    .map(x => x * 2)
    .filter(x => x > 2)
    .tap(sink)
    .subscribe(handler)

  source.publish(1)
  source.publish(2)
  expect(sink.mock.calls).toEqual([[4]])
  expect(handler.mock.calls).toEqual([[4]])
})