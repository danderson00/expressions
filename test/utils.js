const { serializeDefinition, deserializeDefinition } = require('../src/serialization/definition')

// these used to be functions on the serialization API but were deprecated and are now only used in tests
module.exports = {
  stringify: serializationInfo => JSON.stringify(serializeDefinition(serializationInfo)),
  parse: input => deserializeDefinition(JSON.parse(input))
}